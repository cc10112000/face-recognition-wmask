const images = {
    logo: require('../assets/images/logo.svg').default,
    avt: require('../assets/images/Logo_1.png'),
    khoa: require('../assets/images/Khoa1.jpg'),
    duong: require('../assets/images/Duong.png'),
    hieu: require('../assets/images/hieuvip.jpg'),
    quan: require('../assets/images/Quan.png'),
    thao: require('../assets/images/Thao.png')
}

export default images