import React from 'react'
import './about.scss'
import images from "../constants/images"

const About = () => {
    return (
        <div className="about_main">
            <h1 style={{marginBottom: '30px'}}>About my team</h1>
            <div className="profile_row_1">
                <div className="profile-card">
                    <div className="img">
                        <img src={images.hieu} />
                    </div>
                    <div className="caption">
                        <h3>Trung Hiếu</h3>
                        <p>Senior App Developer</p>
                        <div className="social-links">
                            <a href="#"><i className="bx bxl-facebook-square"></i></a>
                            <a href="#"><i className="bx bxl-instagram"></i></a>
                            <a href="#"><i className="bx bxl-twitter"></i></a>
                        </div>
                    </div>
                </div>
                <div className="profile-card">
                    <div className="img">
                        <img src={images.khoa} />
                    </div>
                    <div className="caption">
                        <h3>Đăng Khoa</h3>
                        <p>Fullstack Developer</p>
                        <div className="social-links">
                            <a href="#"><i className="bx bxl-facebook-square"></i></a>
                            <a href="#"><i className="bx bxl-instagram"></i></a>
                            <a href="#"><i className="bx bxl-twitter"></i></a>
                        </div>
                    </div>
                </div>
                <div className="profile-card">
                    <div className="img">
                        <img src={images.quan} />
                    </div>
                    <div className="caption">
                        <h3>Minh Quân</h3>
                        <p>Mobile App Developer</p>
                        <div className="social-links">
                            <a href="#"><i className="bx bxl-facebook-square"></i></a>
                            <a href="#"><i className="bx bxl-instagram"></i></a>
                            <a href="#"><i className="bx bxl-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="profile_row_2">
                <div className="profile-card">
                    <div className="img">
                        <img src={images.thao} />
                    </div>
                    <div className="caption">
                        <h3>Quang Thảo</h3>
                        <p>Web Developer</p>
                        <div className="social-links">
                            <a href="#"><i className="bx bxl-facebook-square"></i></a>
                            <a href="#"><i className="bx bxl-instagram"></i></a>
                            <a href="#"><i className="bx bxl-twitter"></i></a>
                        </div>
                    </div>
                </div>
                <div className="profile-card">
                    <div className="img">
                        <img src={images.duong} />
                    </div>
                    <div className="caption">
                        <h3>Bá Dương</h3>
                        <p>Full Stack Developer</p>
                        <div className="social-links">
                            <a href="#"><i className="bx bxl-facebook-square"></i></a>
                            <a href="#"><i className="bx bxl-instagram"></i></a>
                            <a href="#"><i className="bx bxl-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About