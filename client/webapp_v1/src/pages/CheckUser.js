import React, { useState } from 'react'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';

import './checkuser.scss'




import Webcam from '../components/webcam/webcam'
import UserTable from '../components/tables/userTable'

const CheckUser = () => {

    const [dropdownOpen, setDropdownOpen] = useState(false)
    const toggle = () => setDropdownOpen(prevState => !prevState)

    $(document).ready(function () {
        $('.toggle').click(function (e) {
            e.preventDefault(); // The flicker is a codepen thing
            $(this).toggleClass('toggle-on');
        });
    })
    

    return (
        <div className="checkuser">
            <h1 className="checkuser__title">Check User</h1>
            <div className="checkuser__actions">
                <Webcam />              

            </div>
        </div>
    )
}

export default CheckUser