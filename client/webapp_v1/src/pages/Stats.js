import { React, useEffect, useState, Fragment } from "react";
import axios from "axios";
import Swal from "sweetalert2";
const Stats = () => {
  const [statsData, setStatsData] = useState([]);

  useEffect(() => {
    Swal.fire({
      title: "Loading...",
      didOpen: () => {
        Swal.showLoading();
      },
    });
    axios
      .post("http://127.0.0.1:5000/datalog")
      .then((res) => {
        // console.log(res["data"]);
        setStatsData(res["data"])
        
      })
      .catch((error) => {
        console.log(`error =${error}`);
      });
  }, []);

  console.log(statsData);

  return (
    <div className="stats__content">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Log_id</th>
            <th>User_id</th>
            <th>Checked by</th>
            <th>Room Name</th>
            <th>Check</th>
            <th>Status</th>
            <th>Time</th>
            <th>Date</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          {statsData.map((user, index) => (
            <Fragment key={index}>
              <tr>
                <td>{user[0]}</td>
                <td>{user[1]}</td>
                <td>{user[2]}</td>
                <td>{user[3]}</td>
                <td>{user[4]}</td>
                <td>{user[5]}</td>
                <td>{user[6]}</td>
                <td>{user[7]}</td>
                <td>{user[8]}</td>
              </tr>
              {/* <tr>
                {users.map((user, index) => ( 
                    user[index] === null ? <td>{user[index]}</td> : <td>null</td>
                ))}
              </tr> */}
            </Fragment>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Stats;
