import React, { useEffect, useState, Fragment } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Swal from "sweetalert2";

import axios from "axios";
import useToken from "./useToken";
import jwt_decode from "jwt-decode";
import FormInput from "../components/forms/FormInput";
import "./roomservice.scss";

// import Webcam from '../components/webcam/webcam'

const RoomService = () => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen((prevState) => !prevState);
  const [roomBooking, setRoomBooking] = useState([]);

  const [roomData, setRoomData] = useState([]);
  const [bookingData, setBookingData] = useState([]);
  const { token } = useToken();
  const [isOpen, setIsOpen] = useState(false);
  const [addBookingRoomIsOpen, setAddBookingRoomIsOpen] = useState(false);
  const [addRoomIsOpen, setAddRoomIsOpen] = useState(false);
  const [roomForm, setRoomForm] = useState({
    admin_id: "",
    capacity: "",
    description: "",
  });

  const [bookingForm, setBookingForm] = useState({
    booking_date: "",
    booking_session: ""
  });
  const [editRoomForm, setEditRoomForm] = useState({
    room_id: "",
    room_name: "",
    capacity: "",
    description: "",
  });

  const togglePopup = () => {
    setIsOpen(!isOpen);
  };

  const toggleAddRoomPopup = () => {
    setAddRoomIsOpen(!addRoomIsOpen);
  };

  const toggleAddBookingPopup = () => {
    setAddBookingRoomIsOpen(!addBookingRoomIsOpen);
  };

  useEffect(() => {
    return getRoomBooking();
  }, []);

  const getRoomBooking = React.useCallback(() => {
    axios.get("http://127.0.0.1:5000/getRoom").then((res) => {
      setRoomBooking(res.data);
    });
  });





  const onBookingChange = (e) => {
    setBookingForm({ ...bookingForm, [e.target.name]: e.target.value });
  };

  const addBooking = React.useCallback(
    () => {
      const decoded = jwt_decode(token);
      const room_id = document.getElementById("selectBooking").value;
      console.log(room_id)
      axios({
        method: "POST",
        url: "http://127.0.0.1:5000/bookingRoom",
        data: {
          room_id: room_id,
          admin_id: decoded.sub,
          booking_date: bookingForm.booking_date,
          booking_session: bookingForm.booking_session,
        },
      })
        .then((response) => {
          setRoomData(response.data);
          setBookingForm({
            room_id: "",
            admin_id: "",
            booking_date: "",
            booking_session: "",
          });
          toggleAddRoomPopup();
        })
        .catch((error) => {
          if (error.response) {
            console.log(error.response);
            console.log(error.response.status);
            console.log(error.response.headers);
          }
        });

    },
    [bookingForm]
  );

  // input all form:
  const editInputs = [
    {
      id: 1,
      name: "room_id",
      type: "text",
      placeholder: "Room ID",
      disabled: true,
    },
    {
      id: 2,
      name: "room_name",
      type: "text",
      placeholder: "Room name",
      // label: "room name",
      errorMessage:
        "Room name shouldn't include number and special characters !",
      pattern: "^[A-Za-z]*$",
      required: true,
    },
    {
      id: 3,
      name: "capacity",
      type: "number",
      placeholder: "Capacity",
      // label: "capacity",
      errorMessage: "Capacity only include number !",
      pattern: "^[0-9]*$",
      required: true,
    },
    {
      id: 4,
      name: "description",
      type: "text",
      placeholder: "Description",
      // label: "description",
      // required: true,
    },
  ];

  const addRoomInputs = [
    {
      id: 1,
      name: "room_name",
      type: "text",
      placeholder: "Room name",
      errorMessage: "Room name shouldn't include number and special characters !",
      pattern: "^[A-Za-z]*$",
      required: true,
    },
    {
      id: 2,
      name: "capacity",
      type: "number",
      placeholder: "Capacity",
      errorMessage: "Capacity only include number !",
      pattern: "^[0-9]*$",
      required: true,
    },
    {
      id: 3,
      name: "description",
      type: "text",
      placeholder: "Description",
      // label: "Password",
      // required: true,
    },
  ];
  const addBookingRoomInputs = [
    {
      id: 1,
      name: "booking_date",
      type: "date",
      placeholder: "Booking Date",
      // label: "Password",
      // required: true,
    },
    {
      id: 2,
      name: "booking_session",
      type: "text",
      placeholder: "Booking session",
      // label: "Password",
      // required: true,
    },
  ];



  const addRoom = React.useCallback(
    (e) => {
      console.log(roomForm.room_name);
      console.log(roomForm.capacity);
      console.log(roomForm.description);
      axios({
        method: "POST",
        url: "http://127.0.0.1:5000/addRoom",
        data: {
          room_name: roomForm.room_name,
          capacity: roomForm.capacity,
          description: roomForm.description,
        },
      })
        .then((response) => {
          setRoomData(response.data);
          setRoomForm({
            room_name: "",
            capacity: "",
            description: "",
          });
          toggleAddRoomPopup();
        })
        .catch((error) => {
          if (error.response) {
            console.log(error.response);
            console.log(error.response.status);
            console.log(error.response.headers);
          }
        });
      e.preventDefault();
    },
    [roomForm]
  );
  const callToggle = (room) => {
    togglePopup();
    setEditRoomForm({
      room_id: room.id,
      room_name: room.name,
      capacity: room.capacity,
      description: room.description,
    });
  };
  const handleDeleteRoom = (room_id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          method: "POST",
          url: "http://127.0.0.1:5000/deleteRoom",
          data: {
            room_id: room_id,
          },
        })
          .then((response) => {
            console.log("success");
            setRoomData(response.data);
            Swal.fire(
              "Deleted!",
              `Room id: ${room_id}  has been deleted.`,
              "success"
            );
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  const editRoom = React.useCallback(
    (e) => {
      e.preventDefault();
      console.log(editRoomForm.room_id);
      axios({
        method: "POST",
        url: "http://127.0.0.1:5000/editRoom",
        data: {
          room_name: editRoomForm.room_name,
          capacity: editRoomForm.capacity,
          description: editRoomForm.description,
          room_id: editRoomForm.room_id,
        },
      })
        .then((response) => {
          console.log("success");
          setRoomData(response.data);
          Swal.fire(
            "Update record Successfully!",
            `Room id: ${editRoomForm.room_id}  has been updated.`,
            "success"
          );
          togglePopup()
        })
        .catch((error) => {
          if (error.response) {
            console.log(error.response);
            console.log(error.response.status);
            console.log(error.response.headers);
          }
        });
        e.preventDefault()
    },
    [editRoomForm],[roomData]
  );

  const onChange = (e) => {
    setRoomForm({ ...roomForm, [e.target.name]: e.target.value });
  };

  const onChangeEdit = (e) => {
    setEditRoomForm({ ...editRoomForm, [e.target.name]: e.target.value });
  };

  //get room
  useEffect(() => {
    axios
      .get("http://127.0.0.1:5000/getRoom")
      .then((res) => {
        // console.log(res["data"]);
        setRoomData(res["data"]);
      })
      .catch((error) => {
        console.log(`error =${error}`);
      });
  }, []);
  useEffect(() => {
    axios
      .get("http://127.0.0.1:5000/getRoom")
      .then((res) => {
        // console.log(res["data"]);
        setRoomData(res["data"]);
      })
      .catch((error) => {
        console.log(`error =${error}`);
      });
      axios
      .get("http://127.0.0.1:5000/getBooking")
      .then((res) => {
        // console.log(res["data"]);
        setBookingData(res["data"]);
      })
      .catch((error) => {
        console.log(`error =${error}`);
      });
  }, []);



  return (
    <div style={{ position: "relative" }}>
      <h1 className="room__service__title">Room Service</h1>
      <div className="room__service__content" style={{ position: "relative" }}>
        <table
          className="table table-success table-striped"
          style={{ width: "80%" }}
        >
          <thead>
            <tr>
              <th scope="col">Room ID</th>
              <th scope="col">Room Name</th>
              <th scope="col">Capacity</th>
              <th scope="col">Description</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {roomData.map((room, index) => (
              <Fragment key={index}>
                <tr>
                  <td>{room.id}</td>
                  <td>{room.name}</td>
                  <td>{room.capacity}</td>
                  <td>{room.description}</td>
                  <td>
                    <i
                      className="bx bx-edit"
                      onClick={() => {
                        callToggle(room);
                      }}
                      style={{ fontSize: "x-large", color: "green" }}
                    ></i>
                    <i
                      className="bx bx-trash"
                      onClick={() => handleDeleteRoom(room.id)}
                      style={{ fontSize: "x-large", color: "red" }}
                    ></i>
                  </td>
                </tr>
              </Fragment>
            ))}
          </tbody>
        </table>
        <button
          className="room__service__button1 webcam__button"
          onClick={toggleAddRoomPopup}
        >
          <span>Click!</span>
          <span>Add room!</span>
        </button>
        <button
          className="room__service__button2 webcam__button"
          onClick={toggleAddBookingPopup}
        >
          <span>Click!</span>
          <span>Add booking!</span>
        </button>

        <hr className="line__separate"></hr>
        <table
          className="table table-success table-striped"
          style={{ width: "80%" }}
        >
          <thead>
            <tr>
              <th scope="col">Room Name</th>
              <th scope="col">Admin Name</th>
              <th scope="col">Booking date</th>
              <th scope="col">Booking session</th>
              <th scope="col">Excution time</th>
            </tr>
          </thead>
          <tbody>
            {bookingData.map((booking, index) => (
              <Fragment key={index}>
                <tr>
                  <td>{booking.room}</td>
                  <td>{booking.admin_name}</td>
                  <td>{booking.booking_date}</td>
                  <td>{booking.booking_session}</td>
                  <td>{booking.execution_time}</td>
                </tr>
              </Fragment>
            ))}
          </tbody>
        </table>
        <hr className="line__separate"></hr>
      </div>

      {isOpen && (
        <div className="edit__popup-box">
          <div className="edit__box">
            <form>
              <h1 className="register__title">Edit Room</h1>
              {editInputs.map((input) => (
                <FormInput
                  key={input.id}
                  {...input}
                  value={editRoomForm[input.name]}
                  onChange={onChangeEdit}
                  id={input.name}
                />
              ))}
              <button type="button" className="form__button" onClick={editRoom}>
                Save
              </button>
              <button
                type="button"
                className="form__button"
                onClick={togglePopup}
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      )}
      {addRoomIsOpen && (
        <div className="edit__popup-box">
          <div className="edit__box">
            <form>
              <h1 className="register__title">Add Room</h1>
              {addRoomInputs.map((input) => (
                <FormInput
                  key={input.id}
                  {...input}
                  value={roomForm[input.name]}
                  onChange={onChange}
                  id={input.name}
                />
              ))}
              <button className="form__button" onClick={addRoom}>
                Add Room
              </button>
              <button
                type="button"
                className="form__button"
                onClick={toggleAddRoomPopup}
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      )}
      {addBookingRoomIsOpen && (
        <div className="edit__popup-box">
          <div className="edit__box">
            <form>
              <h1 className="register__title">Add Booking Room</h1>

              <select id="selectBooking">
                <option  defaultValue="NULL">
                  {" "}
                  Select room...{" "}
                </option>
                {roomBooking.map((item) => {
                  return (
                    <option key={item.id} value={item.id}>
                      {item.id + " - " + item.name}
                    </option>
                  );
                })}
              </select>

              {addBookingRoomInputs.map((input) => (
                <FormInput
                  key={input.id}
                  {...input}
                  onChange={onBookingChange}
                  value={bookingForm[input.name]}
                  id={input.name}
                />
              ))}
              <button type='button' className="form__button" onClick={addBooking}>
                Add Booking Room
              </button>
              <button
                type="button"
                onClick={toggleAddBookingPopup}
                className="form__button"
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};

export default RoomService;
