import React, { useState } from 'react'
import Swal from 'sweetalert2' // sweet alert
import './home.scss'
import FormInput from "../components/forms/FormInput";
import Slider from "../components/slider/Slider";
import axios from "axios";
import { Button } from 'reactstrap';
import useToken from "./useToken";

const Home = (props) => {
  const { token, setToken, removeToken } = useToken();

  const [isOpen, setIsOpen] = useState(false);


  const [loginForm, setloginForm] = useState({
    admin_acc: "",
    password: "",
  });
  function logMeOut() {
    const admin_id = token;
    console.log(admin_id)
    axios({
      method: "POST",
      url: "http://127.0.0.1:5000/logout",
    })
      .then((response) => {
        removeToken(response.data.access_token);
        logOutFailAlert();
      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response);
          console.log(error.response.status);
          console.log(error.response.headers);
        }
      });
  }



  const togglePopup = () => {
    setIsOpen(!isOpen);
  };

  const logMeIn = (e) => {

    axios({
      method: "POST",
      url: "http://127.0.0.1:5000/login",
      data: {
        admin_acc: loginForm.admin_acc,
        password: loginForm.password,
      },
    })
      .then((response) => {
        setToken(response.data.access_token);
        togglePopup();
        loginSuccessAlert();
      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response);
          console.log(error.response.status);
          console.log(error.response.headers);
          loginFailAlert();
        }
      });
    setloginForm({
      admin_acc: "",
      password: "",
    });
    e.preventDefault();
  };

  const inputs = [
    {
      id: 1,
      name: "admin_acc",
      type: "text",
      placeholder: "Username",
      errorMessage:
        "Username should be 3-16 characters and shouldn't include any special character!",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },

    {
      id: 2,
      name: "password",
      type: "password",
      placeholder: "Password",
      // label: "Password",
      errorMessage: "Password can not be blank!",
      required: true,
    },

  ];


  // code sweet alert login successful or fail
  const loginSuccessAlert = () => {
    Swal.fire({
      title: 'Login Successfully!!!',
      text: 'Welcome to our website',
      icon: 'success',
    });
  }

  const loginFailAlert = () => {
    Swal.fire({
      title: 'Login fail!!!',
      text: 'Username or password is incorrect',
      icon: 'error'
    });
  }
  const logOutFailAlert = () => {
    Swal.fire({
      title: 'Log out Successfully!!!',
      icon: 'success',
    });
  }


  const iconLoadingImage = () => {
    let timerInterval
    Swal.fire({
      title: 'Auto close alert!',
      html: 'I will close in <b></b> milliseconds.',
      timer: 20000,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading()
        const b = Swal.getHtmlContainer().querySelector('b')
        timerInterval = setInterval(() => {
          b.textContent = Swal.getTimerLeft()
        }, 100)
      },
      willClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        console.log('I was closed by the timer')
      }
    })
  }

  // return (
  //     <div>
  //         {/* <img src={MainImage} alt="facemask-image" className="home__image" /> */}
  //         <Slider />
  //         <div className="mask_check"/>
  const onChange = (e) => {
    setloginForm({ ...loginForm, [e.target.name]: e.target.value });
  };

  return (
    <div style={{ position: 'relative' }}>
      <Slider />

      {!token && token !== "" && token !== undefined ? (
        <Button className="login__button" onClick={togglePopup}>
          Login with admin
        </Button>
      ) : (
        <Button className="login__button" onClick={logMeOut}>
          Log out
        </Button>
      )}
      {isOpen && (
        <div className="home__popup-box">
          <div className="home__box">
            <form>
              <h1 className="register__title">Login</h1>
              {inputs.map((input) => (
                <FormInput
                  key={input.id}
                  {...input}
                  value={loginForm[input.name]}
                  onChange={onChange}
                />
              ))}
              <button className="form__button" onClick={logMeIn}>
                Submit
              </button>
              <button className="form__button" onClick={togglePopup}>
                Cancel
              </button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};
export default Home;
