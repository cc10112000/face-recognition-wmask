import React, { useState, Fragment } from 'react'
import axios from "axios";
import { Input } from 'reactstrap';
import Webcam from "react-webcam";
import Swal from "sweetalert2";


import WebcamComponent from '../components/webcam/webcam'

import './trainfuture.scss'

const TrainFuture = () => {

    const webcamRef = React.useRef(null);
    const [contacts, setContacts] = useState([]);
    const [addFormData, setAddFormData] = useState({
        userID: "",
        userName: "",
    });

    const videoConstraint = {
        width: 500,
        height: 500,
        facingMode: "user",
    };

    
    const checkAlert = (title, text, icon) => {
        Swal.fire({
          title: title,
          text: text,
          icon: icon,
          timer: 2000,
        });
      };

    // console.log(data);

    function validID(value) {
        // var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return value.length >= 8 ?  '' : 'ID must have at least 8 characters!';
    }

    const loadingImg = React.useCallback((event) => {
        event.preventDefault();
        const listCap = [];
        const userid = addFormData.userID
        const username = addFormData.userName
        if (userid === '' || username === '') {
            console.log('id or username is blank!!!!!!!!!')
            console.log(userid + username)
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'User ID or User name is blank!!!!!!!!!',
            })
        } else if (validID(userid)) {
            // console.log(validID(userid));
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: validID(userid),
            })
        }
        const myInterval = setInterval(() => {
            listCap.push(webcamRef.current.getScreenshot());
        }, 300);
        setTimeout(() => {
            clearInterval(myInterval);
            axios
                .post("http://127.0.0.1:5000/trainfeature",
                    {
                        data: listCap,
                        user_id: userid,
                        user_name: username,
                        device: "Web"
                    })
                    .then((res) => {
                        checkAlert("Add successfully!", "New user face is embedded in model", "success")
                    })
                    .catch((error) => {

                        checkAlert("Unexpected error", "May be user id is duplicated, Try again!", "error")

                    });
            }, 3600);

        },

        [webcamRef, addFormData]
    );

    const handleAddFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;

        const newFormData = { ...addFormData };
        newFormData[fieldName] = fieldValue;
        setAddFormData(newFormData);
    }
    // console.log(addFormData)

    const handleAddFormSubmit = (event) => {
        event.preventDefault();

        const newContact = {
            userID: addFormData.userID,
            userName: addFormData.userName,
        };

        const newContacts = [...contacts, newContact];
        setContacts(newContacts);
    }


    return (
        <div className="trainfuture__content">
            <h1>Add a User to Training</h1>
            <Webcam
                className="webcam"
                audio={false}
                height={500}
                ref={webcamRef}
                screenshotFormat="image/jpeg"
                width={500}
                videoConstraints={videoConstraint}
            />
            <form >
                <div style={{ display: 'flex', marginTop: '10px' }}>
                    <Input
                        type="text"
                        name="userID"
                        required={true}
                        placeholder="Enter a ID..."
                        onChange={handleAddFormChange}
                        pattern="^[0-9]*$"
                    />
                    <Input
                        type="text"
                        name="userName"
                        required="required"
                        placeholder="Enter an name..."
                        onChange={handleAddFormChange}
                    />
                    <button className="btn btn-primary" onClick={loadingImg} style={{ margin: 'auto' }}>Add</button>
                </div>

            </form>
        </div>
    )
}

export default TrainFuture