import React, { useRef, useEffect } from 'react'
import './assets/libs/boxicons-2.1.1/css/boxicons.min.css'
import './scss/App.scss'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Blank from './pages/Blank'
import Dashboard from './pages/Dashboard'
import MainLayout from './layout/MainLayout'
import Home from './pages/Home'
import CheckUser from './pages/CheckUser'
import RoomService from './pages/RoomService'
import About from './pages/About'
import Stats from './pages/Stats'
import TrainFuture from './pages/TrainFuture'
import UserTable from './components/tables/userTable'
import $ from 'jquery';


function App() {

  return (  
    // React router
    <BrowserRouter>

      <Routes>

        <Route path="/" element={<MainLayout />}>
          <Route index element={<Home />} />
          <Route path="checkusers" element={<CheckUser />} />
          <Route path="trainfuture" element={<TrainFuture />} />
          <Route path="roomservices" element={<RoomService />} />
          <Route path="about" element={<About />} />
          <Route path="customers" element={<Blank />} />
          <Route path="logout" element={<Blank />} />
          <Route path="stats" element={<Stats />} />
        </Route>
      </Routes>

    </BrowserRouter>
  );
};

export default App;
