

const sidebarNav = [
    {
        link: '/',
        section: 'home',
        icon: <i className='bx bx-home-alt'></i>,
        text: 'Home'
    },
    {
        link: '/checkusers',
        section: 'checkusers',
        icon: <i className='bx bxs-face'></i>,
        text: 'Face Recognition'
    },
    {
        link: '/trainfuture',
        section: 'trainfuture',
        icon: <i className='bx bxs-user-plus'></i>,
        text: 'Train Future'
    },
    {
        link: '/roomservices',
        section: 'roomservices',
        icon: <i className='bx bxs-building'></i>,
        text: 'Room services'
    },
    {
        link: '/about',
        section: 'about',
        icon: <i className='bx bxl-facebook-square'></i>,
        // icon: <i className='bx bx-user'></i>,
        text: 'About Team'
    },
    // {
    //     link: '/customers',
    //     section: 'customers',
    //     icon: <i className='bx bx-receipt' ></i>,
    //     text: 'Customers'
    // },
    {
        link: '/stats',
        section: 'stats',
        icon: <i className='bx bx-line-chart'></i>,
        text: 'Stats'
    }
    // {
    //     link: '/',
    //     section: 'logout',
    //     icon: <i className='bx bx-cog'></i>,
    //     text: 'Log out'
    // }
]

export default sidebarNav