import React, { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'
import './sidebar.scss'
import { images } from '../../constants'
import sidebarNav from '../../configs/sidebarNav'

const Sidebar = () => {
  const [activeIndex, setActiveIndex] = useState(0)
  const location = useLocation()


  useEffect(() => {
    const curPath = window.location.pathname.split('/')[1];
    const activeItem = sidebarNav.findIndex(item => item.section === curPath)

    setActiveIndex(curPath.length === 0 ? 0 : activeItem);
  }, [location])


  const closeSidebar = () => {
    // document.querySelector('.sidebar').style.transform = 'scale(1) translateX(-65%)'
    document.querySelector('.sidebar').style.width = '135px'
    document.querySelector('.sidebar').style.transition = 'all 0.5s ease-in-out'
    document.querySelector('.main__content').style.transform = 'scale(1) translateX(-11%)'
    document.querySelector('.main__content').style.transition = 'all 0.5s ease-in-out'
    // hide text of sidebar
    const sideBarText = document.querySelectorAll('.sidebar__menu__item__txt')
    for (let index = 0; index < sideBarText.length; index++) {
      sideBarText[index].style.display = 'none'
      sideBarText[index].style.transition = 'all 0.5s ease-in-out'
    }

    const sideBarIcon = document.querySelectorAll('.sidebar__menu__item__icon')
    for (let index = 0; index < sideBarIcon.length; index++) {
      sideBarIcon[index].style.transform = 'translateX(20px)'
      sideBarIcon[index].style.transition = 'all 0.5s ease-in-out'
    }
    console.log(sideBarIcon)

    // document.querySelector('.main__content').style.transition = 'all 0.5s ease-in-out'
    // setTimeout(() => {
    //   document.body.classList.remove('sidebar-open')
    //   document.querySelector('.main__content').style = ''
    // }, 500)
  }


  return (

    <div className="sidebar">
      <div className="sidebar__logo">
        <img src={images.logo} alt="" />
        <div className="sidebar-close" onClick={closeSidebar}>
          <i className="bx bx-x"></i>
        </div>
      </div>
      <div className="sidebar__menu">
        {
          sidebarNav.map((nav, index) => (
            <Link to={nav.link} key={`nav-${index}`} className={`sidebar__menu__item ${activeIndex === index && 'active'}`} id={nav.section}>
              <div className="sidebar__menu__item__icon">
                {nav.icon}
              </div>
              <div className="sidebar__menu__item__txt">
                {nav.text}
              </div>
            </Link>
          ))
        }
      </div>
    </div>
  )
}

export default Sidebar