// import SliderImage1 from '.test1.jpg'
import SliderImage2 from './test2.jpg'
import SliderImage3 from './test3.jpg'
import SliderImage4 from './test4.jpg'


export const sliderData = [
  // {
  //   image: "https://i.ibb.co/58Mq6Mb/slide1.jpg",
  //   heading: "Slide One",
  //   desc: "This is the description of slide one Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi quos quas, voluptatum nesciunt illum exercitationem.",
  // },
  // {
  //   image: "https://i.ibb.co/8gwwd4Q/slide2.jpg",
  //   heading: "Slide Two",
  //   desc: "This is the description of slide two Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi quos quas, voluptatum nesciunt illum exercitationem.",
  // },
  // {
  //   image: "https://i.ibb.co/8r7WYJh/slide3.jpg",
  //   heading: "Slide Three",
  //   desc: "This is the description of slide three Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi quos quas, voluptatum nesciunt illum exercitationem.",
  // },
  {
    image: SliderImage2,
    heading: "Face Recognition AI",
    desc: "A facial recognition system is a technology capable of matching a human face from a digital image or a video frame against a database of faces, works by pinpointing and measuring facial features from a given image.",
  },
  {
    image: SliderImage3,
    heading: "Facemask Check",
    desc: "Face Mask Detection Platform uses Artificial Network to recognize if a user is not wearing a mask.  The app can be connected to any existing or new IP mask detection cameras to detect people without a mask. ",
  },
  {
    image: SliderImage4,
    heading: "Check in, Check out",
    desc: "Check in, Check out any time the user wants",
  }
];
