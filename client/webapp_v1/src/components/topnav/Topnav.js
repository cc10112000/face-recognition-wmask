import React from 'react'
import './topnav.scss'
import UserInfo from '../user-info/UserInfo'
import { data } from '../../constants'

const Topnav = () => {

  const openSidebar = () => {
    // document.body.classList.add('sidebar-open')
    // document.querySelector('.sidebar').classList.remove('hide-sidebar')
    document.querySelector('.sidebar').style.transform = 'translateX(0)'
    document.querySelector('.sidebar').style.width = '384px'
    document.querySelector('.main__content').style.transform = 'translateX(0)'

    // hide text of sidebar
    const sideBarText = document.querySelectorAll('.sidebar__menu__item__txt')
    setTimeout(() => { 
      for (let index = 0; index < sideBarText.length; index++) {
        sideBarText[index].style.display = 'block'
        sideBarText[index].style.transition = 'all 0.5s ease-in-out'
      }
     }, 500)
    

    const sideBarIcon = document.querySelectorAll('.sidebar__menu__item__icon')
    for (let index = 0; index < sideBarIcon.length; index++) {
      sideBarIcon[index].style.transform = 'translateX(0%)'
    }
  }

  return (
    <div className="topnav">
      <UserInfo user={data.user} />
      <div className="sidebar-toggle" onClick={openSidebar}>
        <i className="bx bx-menu-alt-right" />
      </div>
    </div>
  )
}

export default Topnav