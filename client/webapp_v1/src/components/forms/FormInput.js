import React from 'react'
import { useState } from "react";
import "./formInput.css";

const FormInput = (props) => {
  const [focused, setFocused] = useState(false);
  const { label, errorMessage, onChange, id, ...inputProps } = props;

  const handleFocus = (e) => {
    setFocused(true);
  };

  return (
    <div className="formInput">
      <label className="form__label">{label}</label>
      <input
        {...inputProps}
        className="form__input"
        onChange={onChange}
        onBlur={handleFocus}
        onFocus={() =>
          inputProps.name === "confirmPassword" && setFocused(true)
        }
        focused={focused.toString()}
        id={id}
      />
      <span className="form__span">{errorMessage}</span>
    </div>
  );
};

export default FormInput;
