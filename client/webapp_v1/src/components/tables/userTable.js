import React, {useState, Fragment } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Input } from 'reactstrap';
import { nanoid } from "nanoid";

import data from './mock-data.json'
import ReadOnlyRow from './ReadOnlyRow';

import './userTable.scss'

const UserTable = () => {


    const [contacts, setContacts] = useState(data);
    const [addFormData, setAddFormData] = useState({
        fullName: "",
        phoneNumber: "",
        email: "",
    });

    const [editFormData, setEditFormData] = useState({
        fullName: "",
        phoneNumber: "",
        email: "",
    });

    const [editContactId, setEditContactId] = useState(null);

    const handleAddFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;

        const newFormData = { ...addFormData };
        newFormData[fieldName] = fieldValue;

        setAddFormData(newFormData);
    };

    const handleEditFormChange = (event) => {
        event.preventDefault();

        const fieldName = event.target.getAttribute("name");
        const fieldValue = event.target.value;

        const newFormData = { ...editFormData };
        newFormData[fieldName] = fieldValue;

        setEditFormData(newFormData);
    };

    const handleAddFormSubmit = (event) => {
        event.preventDefault();

        const newContact = {
            id: nanoid(),
            fullName: addFormData.fullName,
            phoneNumber: addFormData.phoneNumber,
            email: addFormData.email,
        };

        const newContacts = [...contacts, newContact];
        setContacts(newContacts);
    };

    const handleEditFormSubmit = (event) => {
        event.preventDefault();

        const editedContact = {
            id: editContactId,
            fullName: editFormData.fullName,
            phoneNumber: editFormData.phoneNumber,
            email: editFormData.email,
        };

        const newContacts = [...contacts];

        const index = contacts.findIndex((contact) => contact.id === editContactId);

        newContacts[index] = editedContact;

        setContacts(newContacts);
        setEditContactId(null);
    };

    const handleEditClick = (event, contact) => {
        event.preventDefault();
        setEditContactId(contact.id);

        const formValues = {
            fullName: contact.fullName,
            phoneNumber: contact.phoneNumber,
            email: contact.email,
        };

        setEditFormData(formValues);
    };

    const handleCancelClick = () => {
        setEditContactId(null);
    };

    const handleDeleteClick = (contactId) => {
        const newContacts = [...contacts];

        const index = contacts.findIndex((contact) => contact.id === contactId);

        newContacts.splice(index, 1);

        setContacts(newContacts);
    };

    return (
        <div className="app-container">
            <form onSubmit={handleAddFormSubmit}>
                <div style={{display: 'flex'}}>
                    <Input
                        type="text"
                        name="ID"
                        required="required"
                        placeholder="Enter a ID..."
                        onChange={handleAddFormChange}
                    />
                    <Input
                        type="text"
                        name="name"
                        required="required"
                        placeholder="Enter an name..."
                        onChange={handleAddFormChange}
                    />
                    <Input
                        type="text"
                        name="phoneNumber"
                        required="required"
                        placeholder="Enter a phone number..."
                        onChange={handleAddFormChange}
                    />
                    <Input
                        type="email"
                        name="email"
                        required="required"
                        placeholder="Enter an email..."
                        onChange={handleAddFormChange}
                    />
                    <button className="btn btn-primary" type="submit" style={{margin: 'auto'}}>Add</button>
                </div>

            </form>
            <form onSubmit={handleEditFormSubmit}>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {contacts.map((contact, index) => (
                            <Fragment key={index}>
                                <ReadOnlyRow
                                    contact={contact}
                                />
                            </Fragment>
                        ))}
                    </tbody>
                </table>
            </form>

        </div>
    );
}

export default UserTable