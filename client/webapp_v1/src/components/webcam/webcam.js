import React, { useEffect, useState, Fragment } from "react";
import axios from "axios";
import Webcam from "react-webcam";
import "./webcam.scss";
import mockdata from "../tables/mock-data.json";
import "bootstrap/dist/css/bootstrap.min.css";
import useToken from "../../pages/useToken";
import jwt_decode from "jwt-decode";
import Swal from "sweetalert2";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
} from "reactstrap";

import $ from "jquery";

const WebcamCapture = () => {
  const webcamRef = React.useRef(null);
  const { token } = useToken();

  const handleChangeInSelect = (e) => setRoom(e.target.value);
  const [addFormData, setAddFormData] = useState({
    userID: "",
    userName: "",
  });

  const [room, setRoom] = useState([]);
  const videoConstraint = {
    width: 500,
    height: 500,
    facingMode: "user",
  };
  // const [name, setName] = useState("");
  const checkAlert = (title, text, icon) => {
    Swal.fire({
      title: title,
      text: text,
      icon: icon,
      timer: 2000,
    });
  };
  useEffect(() => {
    return getRoom();
  }, []);

  const getRoom = React.useCallback(() => {
    axios.get("http://127.0.0.1:5000/getRoom").then((res) => {
      setRoom(res.data);
    });
  });

  //   function getRoom(){
  // 	  axios.get('http://127.0.0.1:5000/getRoom')
  // 	  .then((res)=>{
  // 		  setRoom(res.data)
  // 	  })
  //   }
  // trả về data khi bấm nút checkin
  const [data, setData] = useState([]);
  const checkin = React.useCallback(
    (event) => {
      event.preventDefault();
      try {
        const imageSrc = webcamRef.current.getScreenshot();
        const decoded = jwt_decode(token);
        const isIn = document.getElementById("a").checked ? "in" : "out";
        axios
          .post("http://127.0.0.1:5000/checkin", {
            data: imageSrc,
            token: decoded.sub,
          })
          .then((res) => {
            // console.log(res["data"]);
            if (res.data.user_name === "Unknown") {
              checkAlert(
                "Unknown user detected!",
                "Please make sure user face in webcam, if new user add before check",
                "error"
              );
            } else {
              checkAlert(
                "Check in Successfully!",
                `${res.data.user_name} check in at ${res.data.datetime}`,
                "success"
              );
            }
            setData((prev) => {
              const newData = [...prev, res.data];
              return newData;
            });
          })
          .catch((error) => {
            checkAlert(error.message, "Please try again!", "error");
          });
      } catch (e) {
        if (e.message === "Invalid token specified") {
          checkAlert(
            "Check in Fail!",
            "Please login by admin account before check in!",
            "error"
          );
        } else {
          checkAlert(e.message, "Please try again!", "error");
        }
      }
    },
    [webcamRef],
    [token]
  );
  const autocheck = async () => {
    
    if (!token && token !== "" && token !== undefined) {
      checkAlert(
        "Check Fail!",
        "Please login by admin account before check in!",
        "error"
      );
    } else {
      const imageSrc = webcamRef.current.getScreenshot();
      const decoded = jwt_decode(token);
      const isIn = document.getElementById("a").checked ? "in" : "out";
      const roomId = document.getElementById("select").value;
      await axios
        .post("http://127.0.0.1:5000/check", {
          data: imageSrc,
          token: decoded.sub,
          devices: "web browser",
          check_status: isIn,
          room_id: roomId,
        })
        .then((res) => {
          if (res.data.user_name === "Unknown") {
            checkAlert(
              "Unknown user detected!",
              "Please make sure user face in webcam, if new user add before check",
              "error"
            );
          } else {
            checkAlert(
              "Check in Successfully!",
              `${res.data.user_name} check ${res.data.check_status} at ${res.data.datetime}`,
              "success"
            );
          }
          if (document.getElementById("d").checked) {
            setTimeout(autocheck, 5000);
          }
        })
        .catch((error) => {
          console.log(error.message);
          if (document.getElementById("d").checked) {
            console.log(document.getElementById("d").checked);
            setTimeout(() => {
              Swal.hideLoading();
              autocheck();
            }, 5000);
          }
        });
    }
  };

  // const checkout = React.useCallback(() => {
  //   try {
  //     const imageSrc = webcamRef.current.getScreenshot();
  //     const decoded = jwt_decode(token);
  //     axios
  //       .post("http://127.0.0.1:5000/checkout", {
  //         data: imageSrc,
  //         token: decoded.sub,
  //       })
  //       .then((res) => {
  //         // console.log(res["data"]);
  //         if (res.data.user_name === "Unknown") {
  //           checkAlert(
  //             "Unknown user detected!",
  //             "Please make sure user face in webcam, if new user add before check",
  //             "error"
  //           );
  //         } else {
  //           checkAlert(
  //             "Check out Successfully!",
  //             `${res.data.user_name} check out at ${res.data.datetime}`,
  //             "success"
  //           );
  //         }
  //         setData((prev) => {
  //           const newData = [...prev, res.data];
  //           return newData;
  //         });
  //       })
  //       .catch((error) => {
  //         checkAlert(error.message, "Please try again!", "error");
  //       });
  //   } catch (e) {
  //     if (e.message === "Invalid token specified") {
  //       checkAlert(
  //         "Check in Fail!",
  //         "Please login by admin account before check in!",
  //         "error"
  //       );
  //     } else {
  //       checkAlert(e.message, "Please try again!", "error");
  //     }
  //   }
  // }, [webcamRef]);

  //add data user để Training

  const handleAddFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...addFormData };
    newFormData[fieldName] = fieldValue;
    setAddFormData(newFormData);
  };
  // console.log(addFormData)

  // console.log(contacts);

  // lấy giá trị thẻ selection
  const startCheck = () => {
    if (!document.getElementById("d").checked) {
      // console.log('1'+document.getElementById("d").checked)
      autocheck();
    }
  };

  // valueSelect()

  return (
    <>
      <Webcam
        className="webcam"
        audio={false}
        height={500}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        width={500}
        videoConstraints={videoConstraint}
      />
      <div className="checkuser__options">
        <div className="selectdiv">
          <label>
            <select id="select">
              <option selected="selected" default value="NULL">
                {" "}
                Check{" "}
              </option>
              {room.map((item) => {
                return (
                  <option key={item.id} value={item.id}>
                    {item.id + " - " + item.name}
                  </option>
                );
              })}
            </select>
          </label>
        </div>
        <div className="can-toggle">
          <input id="a" type="checkbox" />
          <label htmlFor="a">
            <div
              id="toggle-switch"
              className="can-toggle__switch"
              data-checked="In"
              data-unchecked="Out"
            ></div>
          </label>
        </div>
        <div class="can-toggle demo-rebrand-1" id="demo-rebrand-1">
          <input id="d" type="checkbox" />
          <label for="d">
            <div
              class="can-toggle__switch"
              onClick={startCheck}
              data-checked="Start"
              data-unchecked="Stop"
            ></div>
          </label>
        </div>
      </div>
    </>
  );
};
export default WebcamCapture;
