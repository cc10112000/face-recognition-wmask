import React, { useState, useEffect, useRef } from 'react'
import $ from 'jquery';

import './preloader.scss'


const Preloader = () => {

    const preloader = useRef()
    const loader = useRef()

    // $(window).on('load', function () {
    //     $(".loader").fadeOut();
    //     $("#preloader").delay(200).fadeOut("slow");
    // });

    function handleLoader() {
            $(".loader").fadeOut();
            $("#preloader").delay(200).fadeOut("slow");
    }

    useEffect(() => {
        window.addEventListener('load', handleLoader)

        return () => {
            window.removeEventListener("load", handleLoader)
        }
    }, [])

    return (
        <div id="preloader" ref={preloader}>
            <div className="loader" ref={loader}></div>
        </div>
    )
}

export default Preloader