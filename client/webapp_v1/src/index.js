import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import Preloader from './components/preloader/Preloader'

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);