import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Camera, useCameraDevices} from 'react-native-vision-camera';
import SignInScreen from './src/screens/SignInScreen';
import AddUserScreen from './src/screens/AddUserScreen';
import ListUserScreen from './src/screens/ListUserScreen/ListUserScreen';
import AdminScreen from './src/screens/ProfileScreen/AdminScreen';
import UserScreen from './src/screens/ProfileScreen/UserScreen';
import Navigation from './src/navigation';

// import CamScreen from './src/screens/CameraScreen/camScreen';

const App = () => {

  return (
    <SafeAreaView style={styles.root}>
      <Navigation />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});

export default App;
