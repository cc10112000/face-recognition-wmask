import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import useToken from '../../screens/SignInScreen/useToken';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import { AuthContext } from '../Context';

const Header = () => {
    const { removeToken, token } = useToken();
    const navigation = useNavigation();

    const { signOut } = React.useContext(AuthContext)

    const onSelectCameraPressed = () => {
        console.warn("Select Camera")
        navigation.navigate('SelectCamera')
    }

    return (
        <View>
            <View style={styles.header}>
                <View style={styles.headline}>
                    <Text style={styles.scan} onPress={onSelectCameraPressed}><FontAwesome5 name={'camera-retro'} color="#fff" solid size={30} /></Text>
                    <Text style={styles.title}>Admin</Text>
                    <Text style={styles.logout} onPress={() => { signOut() }}><FontAwesome5 name={'sign-out-alt'} color="#fff" solid size={30} /></Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        marginTop: '2%',
        height: '58%',
        width: '130%',
        backgroundColor: '#2CBDFB',
        borderRadius: 20
    },
    headline: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: '2%',
    },
    title: {
        fontSize: 40,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: '20%',
        marginRight: '20%',
    },
    scan: {
        textAlignVertical: 'center',
    },
    logout: {
        textAlignVertical: 'center',
    },
});



export default Header;