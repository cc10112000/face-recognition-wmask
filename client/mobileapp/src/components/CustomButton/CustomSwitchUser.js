import React, { useState } from 'react';
import { Text, View, TouchableOpacity, SafeAreaView } from 'react-native';
import InformationUser from '../../screens/ProfileScreen/InformationUser';
import SettingsUser from '../../screens/ProfileScreen/SettingsUser';

const CustomSwitchUser = ({
    selectionMode,
    roundCorner,
    option1,
    option2,
    onSelectSwitch,
    selectionColor
}) => {
    const [getSelectionMode, setSelectionMode] = useState(selectionMode);
    const [getRoundCorner, setRoundCorner] = useState(roundCorner);

    const updatedSwitchData = val => {
        setSelectionMode(val);
        onSelectSwitch(val);
    };

    const [index, setIndex] = useState(1);
    const RenderElement = () => {
        //You can add N number of Views here in if-else condition
        if (index === 1) {
            //Return the FirstScreen as a child to set in Parent View
            return <InformationUser />;
        } else {
            //Return the SecondScreen as a child to set in Parent View
            return <SettingsUser />;
        }
    };

    
    
    return (
        <SafeAreaView>
            <View
                style={{
                    height: 44,
                    width: '100%',
                    backgroundColor: '#F6F6F6',
                    borderRadius: getRoundCorner ? 25 : 0,
                    borderWidth: 1,
                    borderColor: selectionColor,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    padding: 2,
                }}>
                    
                <TouchableOpacity
                    activeOpacity={1}                  
                    onPress={() => {setIndex(1), updatedSwitchData(1)}}
                    style={{
                        flex: 1,
                        backgroundColor: getSelectionMode == 1 ? selectionColor : '#F6F6F6',
                        borderRadius: getRoundCorner ? 25 : 0,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            color: getSelectionMode == 1 ? '#fff' : selectionColor,
                        }}>
                        {option1}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    TouchableOpacity
                    activeOpacity={1}             
                    onPress={() => {setIndex(2), updatedSwitchData(2)}}
                    style={{
                        flex: 1,
                        backgroundColor: getSelectionMode == 2 ? selectionColor : '#F6F6F6',
                        borderRadius: getRoundCorner ? 25 : 0,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            color: getSelectionMode == 2 ? '#fff' : selectionColor,
                        }}>
                        {option2}
                    </Text>
                </TouchableOpacity>
            </View>
            <RenderElement />
        </SafeAreaView>
    );
};
export default CustomSwitchUser;