import { View, Text, StyleSheet, Pressable } from 'react-native'
import React from 'react'

const CustomButtonIcon = ({ onPress, icon, text, type = "PRIMARY" }) => {
  return (
    <Pressable onPress={onPress} style={[styles.container, styles[`container_${type}`]]}>
      <View style={styles.viewIcon}>{icon}</View>
      <Text style={[styles.text, styles[`text_${type}`]]}>{text}</Text>
    </Pressable>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 30,
    fontFamily: 'Inter',
  },

  container_PRIMARY: {
    backgroundColor: '#2CBDFB',
  },

  container_TERTIARY: {},

  text: {
    fontWeight: 'bold',
    color: 'white',
    flex: 3
  },

  text_TERTIARY: {
    color: '#2CBDFB',
  },
  viewIcon:{
    flex: 1.5,
    placeItem: 'center'
  }
})

export default CustomButtonIcon