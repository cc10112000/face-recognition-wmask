import { View, Text, TextInput, StyleSheet } from 'react-native'
import React from 'react'

const CustomInput = ({ value, setValue, placeholder, secureTextEntry }) => {
    return (
        <View style={styles.container}>
            <TextInput
                value={value}
                onChangeText={setValue}
                placeholder={placeholder}
                style={styles.input}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    searchIcon: {
        padding: 10,
    },
    container: {

        flexDirection: 'row',
        backgroundColor: '#F6F6F6',
        width: '100%',

        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 10,

        paddingHorizontal: 10,
        marginVertical: 5,

    },
    input: {},
})

export default CustomInput