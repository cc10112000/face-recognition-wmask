import { View, Text, Image, StyleSheet, SafeAreaView, ActivityIndicator, FlatList, Animated, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import { onChange } from 'react-native-reanimated';
import { useNavigation } from '@react-navigation/native';
const HEIGHT_IMG = 50;
const ITEM_PADDING = 10;
const ITEM_MARGIN_BOTTOM = 8;
const ITEM_SIZE = HEIGHT_IMG + ITEM_PADDING * 2 + ITEM_MARGIN_BOTTOM;


const ListUserInRoom = () => {

    const navigation = useNavigation();

    const [data, setData] = useState([]);
    const [isLoading, setisLoading] = useState(true);

    const scrollY = React.useRef(new Animated.Value(0)).current;

    useEffect(() => {
        getListPhotos();
        return () => {

        }
    }, []);

    getListPhotos = () => {
        const apiURL = 'http://10.0.2.2:5000/datalog';
        fetch(apiURL)
            .then((res) => res.json())
            .then((resJson) => {
                setData(resJson)
                console.log(resJson)
            }).catch((error) => {
                console.log('Request API Error: ', error)
            }).finally(() => setisLoading(false))
    }

    renderItem = ({ item, index }) => {

        const changeColor = () => {
            if (item[4] === 'No Mask') {
                return 1;
            }
            if (item[4] === 'Masked') {
                return 2;
            }
        }

        const scale = scrollY.interpolate({
            inputRange: [
                -1, 0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + 2)
            ],
            outputRange: [1, 1, 1, 0]
        })

        const opacity = scrollY.interpolate({
            inputRange: [
                -1, 0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + .6)
            ],
            outputRange: [1, 1, 1, 0]
        })


        return (
            <TouchableOpacity>
                <Animated.View
                    style={[
                        styles.item,
                        {
                            transform: [{ scale }],
                            opacity
                        }
                    ]}>

                    <View style={styles.wrapText} >
                        <Text style={styles.fontId} >{item[1]}</Text>
                    </View>
                    <View>
                        <Text style={[
                            (changeColor(item[4]) == 1) ? styles.red : styles.fontCheck,
                            (changeColor(item[4]) == 2) ? styles.green : styles.fontCheck,
                            styles.fontCheck]}>{item[4]}</Text>
                    </View>
                    <View style={styles.dateTime} >
                        <Text style={styles.fontSize}>Time: {item[5]}</Text>
                        <Text style={styles.fontSize}>Date: {item[6]}</Text>
                    </View>
                </Animated.View>
            </TouchableOpacity>
        )
    }
    return (
        <View style={styles.container} >
            {
                isLoading ? <ActivityIndicator /> : (
                    <Animated.FlatList
                        data={data}
                        renderItem={renderItem}
                        keyExtractor={item => `key-${item[1]}`}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: scrollY } } }],
                            { useNativeDriver: true }
                        )}
                    />
                )
            }
        </View>
    )
}

const styles = StyleSheet.create({
    fontCheck: {
        fontSize: 14
    },
    green: {
        color: 'green'
    },

    red: {
        color: 'red'
    },

    container: {
        flexDirection: 'row',
        width: '100%',
        height: '110%',
        paddingHorizontal: 10,
        marginVertical: 5,

    },

    image: {
        width: 50,
        height: HEIGHT_IMG,
        borderRadius: 180,
        paddingLeft: 0,
        paddingRight: 0,
        marginLeft: -5,
    },

    dateTime: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: '10%',
    },

    wrapText: {
        flex: 1,
        justifyContent: 'center',
    },

    fontId: {
        textTransform: 'uppercase',
        fontWeight: '900',
        fontSize: 14,
    },

    fontSize: {
        fontSize: 14,
    },

    item: {
        alignItems: 'center',
        height: 80,
        flexDirection: 'row',
        marginBottom: ITEM_MARGIN_BOTTOM,
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: .3,
        shadowRadius: 20,
        padding: 10,
        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 10,
        padding: ITEM_PADDING
    }
});
export default ListUserInRoom