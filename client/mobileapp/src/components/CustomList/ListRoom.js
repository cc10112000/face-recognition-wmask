import { View, Text, StyleSheet, SafeAreaView, ActivityIndicator, Animated, TouchableOpacity, RefreshControl, TextInput } from 'react-native';
import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
const HEIGHT_IMG = 150;
const ITEM_PADDING = 10;
const ITEM_MARGIN_BOTTOM = 8;
const ITEM_SIZE = HEIGHT_IMG + ITEM_PADDING * 2 + ITEM_MARGIN_BOTTOM;


const ListRoom = () => {

    const navigation = useNavigation();
    const onRoomManageScreenPressed = () => {
        console.warn("ManageRoom")
        navigation.navigate('ManageRoom')
    }
    const [filterData, setfilterData] = useState([]);
    const [data, setData] = useState([]);
    const [search, setSearch] = useState('');
    const [refreshing, setrefreshing] = useState(true);

    const scrollY = React.useRef(new Animated.Value(0)).current;

    useEffect(() => {
        getListRoom();
        return () => {

        }
    }, []);

    const searchFilter = (text) => {
        if (text) {
            const newData = data.filter((item) => {
                const itemData = item.name ? item.name.toUpperCase()
                    : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
            setfilterData(newData);
            setSearch(text);
        } else {
            setfilterData(data);
            setSearch(text);
        }
    }

    getListRoom = () => {
        const apiURL = 'http://10.0.2.2:5000/getRoom';
        fetch(apiURL)
            .then((res) => res.json())
            .then((resJson) => {
                setfilterData(resJson)
                setData(resJson)
                console.log(resJson)
            }).catch((error) => {
                console.log('Request API Error: ', error)
            }).finally(() => setrefreshing(false))
    }

    renderItem = ({ item, index }) => {

        const scale = scrollY.interpolate({
            inputRange: [
                -1, 0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + 2)
            ],
            outputRange: [1, 1, 1, 0]
        })

        const opacity = scrollY.interpolate({
            inputRange: [
                -1, 0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + .6)
            ],
            outputRange: [1, 1, 1, 0]
        })


        return (
                <Animated.View
                    style={[
                        styles.item1,
                        {
                            transform: [{ scale }],
                            opacity
                        }
                    ]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.fontId, { flex: 3.5 }]} >ID Room: {item.id}</Text>
                        <Text style={[styles.fontId, { flex: 4 }]} >Number of people: {item.capacity}</Text>
                    </View>
                    <View>
                        <Text style={styles.fontId}>Room Name: {item.name}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.fontId]}>Description: {item.description}</Text>
                    </View>
                </Animated.View>
        )
    }

    const onRefresh = () => {
        setData([])
        getListRoom()
    }
    return (
        <SafeAreaView>
            <View style={{flexDirection: 'row'}}>
            <TextInput style={styles.textInputStyle}
                    placeholder="Search by Room"
                    value={search}
                    onChangeText={(text) => searchFilter(text)}
                />
            </View>
            <View style={styles.container} >
            {
                refreshing ? <ActivityIndicator /> : (
                    <Animated.FlatList
                        data={filterData}
                        renderItem={renderItem}
                        keyExtractor={item => `key-${item.id}`}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: scrollY } } }],
                            { useNativeDriver: true }
                        )}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                            />
                        }
                    />
                )
            }
        </View>
        </SafeAreaView>
        
    )
}

const styles = StyleSheet.create({
    fontCheck: {
        fontSize: 14
    },
    green: {
        color: 'green'
    },

    red: {
        color: 'red'
    },

    container: {
        flexDirection: 'row',
        width: '100%',
        height: '80%',
        paddingHorizontal: 10,
        marginVertical: 5,

    },

    image: {
        width: 50,
        height: HEIGHT_IMG,
        borderRadius: 180,
        paddingLeft: 0,
        paddingRight: 0,
        marginLeft: -5,
    },

    dateTime: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: '10%',
    },

    wrapText: {
        flex: 1,
        justifyContent: 'center',
    },

    fontId: {
        fontWeight: '900',
        fontSize: 14,
        margin: 2
    },

    fontSize: {
        fontSize: 14,
    },

    item1: {
        alignItems: 'flex-start',
        height: 110,
        justifyContent: 'center',
        marginBottom: ITEM_MARGIN_BOTTOM,
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: .3,
        shadowRadius: 20,
        padding: 10,
        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 10,
        padding: ITEM_PADDING
    },
    textInputStyle:{
        flexDirection: 'row',
        backgroundColor: '#F6F6F6',
        width: '100%',

        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 30,

        paddingHorizontal: 10,
        marginVertical: 5,
    }
});
export default ListRoom