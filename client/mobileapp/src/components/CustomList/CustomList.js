import {
    View, Text, StyleSheet, SafeAreaView, ActivityIndicator, FlatList, Animated,
    TouchableOpacity, RefreshControl, TextInput
} from 'react-native';
import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
const HEIGHT_IMG = 50;
const ITEM_PADDING = 10;
const ITEM_MARGIN_BOTTOM = 8;
const ITEM_SIZE = HEIGHT_IMG + ITEM_PADDING * 2 + ITEM_MARGIN_BOTTOM;


const CustomList = () => {

    const navigation = useNavigation();
    const onUserScreenPressed = (item) => {
        navigation.navigate('ManageUser')
    }

    const [filterData, setfilterData] = useState([]);
    const [data, setData] = useState([]);
    const [search, setSearch] = useState('');
    const [refreshing, setrefreshing] = useState(true);

    const scrollY = React.useRef(new Animated.Value(0)).current;

    useEffect(() => {
        getListLog();
        return () => {
        }
    }, []);

    const searchFilter = (text) => {
        if (text) {
            const newData = data.filter((item) => {
                const itemData = item[3] ? item[3].toUpperCase()
                    : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
            setfilterData(newData);
            setSearch(text);
        } else {
            setfilterData(data);
            setSearch(text);
        }
    }

    getListLog = () => {
        const apiURL = 'http://10.0.2.2:5000/datalog';
        fetch(apiURL)
            .then((res) => res.json())
            .then((resJson) => {
                setfilterData(resJson)
                setData(resJson)

                console.log(resJson)
            }).catch((error) => {
                console.log('Request API Error: ', error)
            }).finally(() => setrefreshing(false))
    }

    renderItem = ({ item, index }) => {
        const changeColor = () => {
            if (item[5] === 'No Mask') {
                return 1;
            }
            if (item[5] === 'Masked') {
                return 2;
            }
        }

        const scale = scrollY.interpolate({
            inputRange: [
                -1, 0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + 2)
            ],
            outputRange: [1, 1, 1, 0]
        })

        const opacity = scrollY.interpolate({
            inputRange: [
                -1, 0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + .6)
            ],
            outputRange: [1, 1, 1, 0]
        })


        return (
            <TouchableOpacity onPress={() => {
                console.log(item[1])
                axios.post('http://10.0.2.2:5000/userprofile', { user_id: item[1] })
                    .then((res) => {
                        console.log(res.data)
                        navigation.navigate('ManageUser', res.data)
                    })


            }}>
                <Animated.View
                    style={[
                        styles.item,
                        {
                            transform: [{ scale }],
                            opacity
                        }
                    ]}>
                    <View style={{ width: '100%', flexDirection: 'row' }}>
                        <View style={[{ flexDirection: 'row', marginTop: '3%' }]} >
                            <Text>Room: </Text>
                            <Text style={styles.fontId}>{item[3].toUpperCase()}</Text>
                        </View>
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row' }}>
                        <View style={[{ flexDirection: 'row' }]} >
                            <Text>User Id: </Text>
                            <Text style={styles.fontId}>{item[1]}</Text>
                        </View>
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row' }}>
                        <View>
                            <View style={[{ flexDirection: 'row' }]} >
                                <Text>Check: </Text>
                                <Text style={styles.fontId}>{item[4]}</Text>
                            </View>
                            <View style={[{ flexDirection: 'row' }]}>
                                <Text>Status: </Text>
                                <Text style={[
                                    (changeColor(item[5]) == 1) ? styles.red : styles.fontCheck,
                                    (changeColor(item[5]) == 2) ? styles.green : styles.fontCheck,
                                    styles.fontCheck]}>{item[5]}</Text>
                            </View>
                        </View>
                        <View style={styles.dateTime} >
                            <Text style={styles.fontSize}>Time: {item[6]}</Text>
                            <Text style={styles.fontSize}>Date: {item[7]}</Text>
                        </View>
                    </View>

                </Animated.View>
            </TouchableOpacity>
        )
    }

    const onRefresh = () => {
        setData([])
        getListLog()
    }

    return (
        <SafeAreaView>
            <View style={{flexDirection: 'row'}}>
            <TextInput style={styles.textInputStyle}
                    placeholder="Search by Room"
                    value={search}
                    onChangeText={(text) => searchFilter(text)}
                />
            </View>
            <View style={styles.container} >
                {
                    refreshing ? <ActivityIndicator /> : (
                        <Animated.FlatList
                            data={filterData}
                            renderItem={renderItem}
                            keyExtractor={item => `key-${item[1]}`}
                            onScroll={Animated.event(
                                [{ nativeEvent: { contentOffset: { y: scrollY } } }],
                                { useNativeDriver: true }
                            )}
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={onRefresh}
                                />
                            }
                        />
                    )
                }
            </View>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    fontCheck: {
        fontSize: 14
    },
    green: {
        color: 'green'
    },

    red: {
        color: 'red'
    },

    container: {
        flexDirection: 'row',
        width: '100%',
        height: '80%',
        paddingHorizontal: 10,
        marginVertical: 5,

    },

    image: {
        width: 50,
        height: HEIGHT_IMG,
        borderRadius: 180,
        paddingLeft: 0,
        paddingRight: 0,
        marginLeft: -5,
    },

    dateTime: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: '10%',
    },

    wrapText: {
        flex: 1,
        justifyContent: 'center',
    },

    fontId: {
        textTransform: 'uppercase',
        fontWeight: '900',
        fontSize: 14,
    },

    fontSize: {
        fontSize: 14,
    },

    item: {
        alignItems: 'center',
        height: 110,
        marginBottom: ITEM_MARGIN_BOTTOM,
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: .3,
        shadowRadius: 20,
        padding: 10,
        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 10,
        padding: ITEM_PADDING
    },
    row: {
        marginLeft: '-11%'
    },
    textInputStyle:{
        flexDirection: 'row',
        backgroundColor: '#F6F6F6',
        width: '100%',

        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 30,

        paddingHorizontal: 10,
        marginVertical: 5,
    }
});
export default CustomList