import React from 'react'
import {
    StyleSheet, Text, View,
    TouchableOpacity, Dimensions, ScrollView
} from 'react-native'

const OPTIONSS = [
    [
        3,
        "404 Group",
        25,
        "Trung sinh"
    ],
    [
        5,
        "SE1403",
        30,
        "Le tot nghiep"
    ],
    [
        6,
        "SE1404",
        30,
        "Le tot nghiep"
    ],
    [
        2,
        "SE1409",
        30,
        "Le tot nghiep"
    ]
]
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const ListId = (props) => {

    const onPressItem = (option1) =>{
        props.changeModalVisibility(false);
        props.setDataList(option1);
    }

    const option1 = OPTIONSS.map((item, index) => {
        return (
            <TouchableOpacity
                style={styles.option}
                key={index}
                onPress={() => onPressItem(item)}
            >
                <Text style={styles.text}>
                    {item}
                </Text>
            </TouchableOpacity>
        )
    })

    return (
        <TouchableOpacity onPress={() => props.changeModalVisibility(false)}
            style={styles.container}>
            <View style={[styles.modal, { width: WIDTH - 20, height: HEIGHT / 4 }]}>
                <ScrollView>
                    {option1}
                </ScrollView>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    modal: {
        backgroundColor: 'white',
        borderRadius: 10
    },
    option:{
        alignItems: 'flex-start'
    },
    text:{
        margin: 20,
        fontSize: 20,
        fontWeight: 'bold'
    }
})

export { ListId };