import {useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

function useToken() {
  function getToken() {
    const userToken = AsyncStorage.getItem('token');
    return userToken && userToken;
  }

  const [token, setToken] = useState(getToken());

  function saveToken(userToken) {
    AsyncStorage.setItem('token', userToken);
    setToken(userToken);
  }

  function removeToken() {
    AsyncStorage.removeItem('token');
    setToken(null);
  }


  return {
    setToken: saveToken,
    token,
    removeToken,
  };
}

export default useToken;
