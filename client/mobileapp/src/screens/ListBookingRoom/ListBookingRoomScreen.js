import React from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';
import ListBooking from './ListBooking';
import { useNavigation } from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';



const ListBookingRoomScreen = () => {

  const navigation = useNavigation();

  const onBackPressed = () => {
    console.warn("Manage User")
    navigation.navigate('AdminScreen')
  }

  const onCreatePressed = () => {
    console.warn("Manage User")
    navigation.navigate('ListBooking')
  }

  return (
    <SafeAreaView>
      <View style={styles.root}>
        <View style={styles.headline}>
          <FontAwesome5 style={styles.back} name={'angle-left'} color='#2CBDFB' solid size={35} onPress={onBackPressed}/>
          <Text style={styles.title} >List Booking Room</Text>
          {/* <FontAwesome5 style={styles.createRoom} name={'plus-square'} color='#2CBDFB' solid size={35} onPress={onCreatePressed}/> */}
        </View>
        <ListBooking />
      </View>
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  back: {
    textAlignVertical: 'center',
  },
  createRoom:{
    textAlignVertical: 'center',
  },

  headline: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: '5%',
    width: '100%',
    justifyContent: 'center'
  },

  title: {
    textAlignVertical: 'center',
    alignItems:'center',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#2CBDFB',
    marginLeft: '10%',
    marginRight: '10%'
  },

});

export default ListBookingRoomScreen;
