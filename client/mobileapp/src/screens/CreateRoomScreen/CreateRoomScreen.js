import React, { useState, Component } from 'react';
import { View, Text, Image, StyleSheet, Button, TouchableOpacity, Modal, TextInput, TextBase, StatusBar, Alert } from 'react-native';
import CustomInput from '../../components/CustomInput/CustomInput';
import CustomButton from '../../components/CustomButton/CustomButton';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { useNavigation } from '@react-navigation/native';
import DateTimePicker from "react-native-modal-datetime-picker";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import { ModalPicker, ListId } from '../../components/DropButton/ModalPicker';
import { ModalPicker } from '../../components/DropButton/ModalPicker';
import { ListId } from '../../components/DropButton/ListId';
import { color } from 'react-native-reanimated';
import { useTheme } from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';

const CreateRoomScreen = () => {

  const navigation = useNavigation();
  const { colors } = useTheme();

  const [data, setData] = React.useState({
    nameRoom: '',
    quantity: '',
    description: '',
    check_textInputChange: false,
    check_textInputChange1: false,
    isValidNameRoom: true,
    isValidQuantity: true,
  });

  const textInputChange = (val) => {
    if (!isNaN(val) == false && val.match(/\d+/g) == null) {
      setData({
        ...data,
        nameRoom: val,
        check_textInputChange: true,
        isValidNameRoom: true
      });
    } else {
      setData({
        ...data,
        nameRoom: val,
        check_textInputChange: false,
        isValidNameRoom: false
      });
    }
  }

  const handleValidName = (val) => {
    if (!isNaN(val) == false && val.match(/\d+/g) == null) {
      setData({
        ...data,
        isValidNameRoom: true
      });
    } else {
      setData({
        ...data,
        isValidNameRoom: false
      });
    }
  }

  const textInputChangeQuantity = (val) => {
    if (val.trim().length >= 1 && !isNaN(val) == true) {
      setData({
        ...data,
        quantity: val,
        check_textInputChange1: true,
        isValidQuantity: true
      });
    } else {
      setData({
        ...data,
        quantity: val,
        check_textInputChange1: false,
        isValidQuantity: false
      });
    }
  }

  const handleValidQuantity = (val) => {
    if (val.trim().length >= 1 && !isNaN(val) == true) {
      setData({
        ...data,
        isValidQuantity: true
      });
    } else {
      setData({
        ...data,
        isValidQuantity: false
      });
    }
  }

  const textInputDescription = (val) => {
    setData({
      ...data,
      description: val,
    });
  }
  const handleValidDescription = (val) => {
    setData({
      ...data,
    });
  }

  const CreatHandle = async (nameRoom, quantity, description) => {
    if (data.nameRoom.match(/\d+/g) != null || data.nameRoom.length == 0) {
      Alert.alert('Wrong Input!', 'Room Name field cannot be empty and only characters.', [
        { text: 'Okay' }
      ]);
      return;
    } if (isNaN(data.quantity) == true || data.quantity.length == 0 || data.nameRoom.match(/\d+/g) != null) {
      Alert.alert('Wrong Input!', 'Capacity field cannot be empty and only integer.', [
        { text: 'Okay' }
      ]);
      return;
    } else {
      await axios.post('http://10.0.2.2:5000/addRoom', {
        room_name: nameRoom,
        capacity: quantity,
        description: description
      }).then(() => {
      }).catch((err) => {
        console.log(err)
      })
      Alert.alert('Add Room', 'Successfully!!!', [
        { text: 'Okay' }
      ]);
    }
    console.log(nameRoom)
    console.log(quantity)
    console.log(description)
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#2CBDFB' barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.title}>Create Meeting Room</Text>
      </View>
      <Animatable.View
        animation="fadeInUpBig"
        style={[styles.footer, {
          backgroundColor: colors.background
        }]}
      >
        <Text style={[styles.text_footer, {
          color: colors.text
        }]}>Room Name</Text>
        <View style={styles.action}>
          <FontAwesome
            name="building-o"
            color={colors.text}
            size={20}
          />
          <TextInput
            placeholder="Room Name"
            placeholderTextColor="#666666"
            style={[styles.textInput, {
              color: colors.text
            }]}
            autoCapitalize="none"
            onChangeText={(val) => textInputChange(val)}
            onEndEditing={(e) => handleValidName(e.nativeEvent.text)}
          />
          {data.check_textInputChange ?
            <Animatable.View
              animation="bounceIn"
            >
              <Feather
                name="check-circle"
                color="green"
                size={20}
              />
            </Animatable.View>
            : null}
        </View>
        {data.isValidNameRoom ? null :
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Only characters</Text>
          </Animatable.View>
        }

        <Text style={[styles.text_footer, {
          color: colors.text
        }]}>Capacity</Text>
        <View style={styles.action}>
          <FontAwesome
            name="child"
            color={colors.text}
            size={20}
          />
          <TextInput
            placeholder="Capacity"
            placeholderTextColor="#666666"
            style={[styles.textInput, {
              color: colors.text
            }]}
            autoCapitalize="none"
            onChangeText={(val) => textInputChangeQuantity(val)}
            onEndEditing={(e) => handleValidQuantity(e.nativeEvent.text)}
          />
          {data.check_textInputChange1 ?
            <Animatable.View
              animation="bounceIn"
            >
              <Feather
                name="check-circle"
                color="green"
                size={20}
              />
            </Animatable.View>
            : null}
        </View>
        {data.isValidQuantity ? null :
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Only Integer</Text>
          </Animatable.View>
        }

        <Text style={[styles.text_footer, {
          color: colors.text
        }]}>Description</Text>
        <View style={styles.action}>
          <FontAwesome
            name="file-text-o"
            color={colors.text}
            size={20}
          />
          <TextInput
            placeholder="Description"
            placeholderTextColor="#666666"
            style={[styles.textInput, {
              color: colors.text
            }]}
            autoCapitalize="none"
            onChangeText={(val) => textInputDescription(val)}
            onEndEditing={(e) => handleValidDescription(e.nativeEvent.text)}
          />
        </View>

        <View style={styles.button}>
          <TouchableOpacity
            style={styles.create}
            onPress={() => { CreatHandle(data.nameRoom, data.quantity, data.description) }}
          >
            <LinearGradient
              colors={['#2CBDFB', '#27aae1']}
              style={styles.create}
            >
              <Text style={[styles.textSign, {
                color: '#fff'
              }]}>Create</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('ListRoom')}
            style={[styles.create, {
              borderColor: '#2CBDFB',
              borderWidth: 1,
              marginTop: 15
            }]}
          >
            <Text style={[styles.textSign, {
              color: '#2CBDFB'
            }]}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 20,
  },

  headline: {
    width: '100%',
    flexDirection: 'row',
  },

  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff',
    margin: 10,
    marginBottom: 30,
  },
  // selectBox: {
  //   flexDirection: 'row',
  //   backgroundColor: '#F6F6F6',
  //   width: '100%',
  //   height: 50,
  //   borderColor: '#E8E8E8',
  //   borderWidth: 2,
  //   borderRadius: 10,
  //   fontSize: 15,
  //   alignItems: 'center',
  //   paddingHorizontal: 10,
  //   marginTop: 0
  // },
  // dateTime: {
  //   flexDirection: 'row',
  //   backgroundColor: '#F6F6F6',
  //   width: '100%',
  //   height: 50,
  //   borderColor: '#E8E8E8',
  //   borderWidth: 2,
  //   borderRadius: 10,
  //   fontSize: 15,
  //   alignItems: 'center',
  //   paddingHorizontal: 10,
  //   marginVertical: 5,
  // },
  container: {
    flex: 2,
    backgroundColor: '#2CBDFB'
  },
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingBottom: 10
  },
  footer: {
    flex: 6,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50
  },
  create: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold'
  }
});


export default CreateRoomScreen;


