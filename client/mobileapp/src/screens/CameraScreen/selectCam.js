import { View, Text, StyleSheet, Image, Dimensions } from 'react-native'
import Avatar from '../../../assets/image/Admin1.png';
import { CustomButtonIcon } from '../../components/CustomButton';
import HeaderSelectCam from '../../components/Header/HeaderSelectCam';
import React from 'react'
import { useNavigation } from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


const SelectCam = () => {

    const navigation =useNavigation();

    const onCheckInPressed = () => {
        console.warn("Check In")
        navigation.navigate('CheckIn')
    }

    const onCheckOutPressed = async () => {
        console.warn("Check Out")
        navigation.navigate('CheckOut')

    }
    const onAddNewPressed = () => {
        console.warn("Add New")
        navigation.navigate('AddNew')
    }
    
    const { avatar, container, button } = styles;
    return (
        <View style={container}>
            <HeaderSelectCam />
            <Image
                style={avatar}
                source={Avatar}
            />
            <View style={button}>
                <CustomButtonIcon text="Check In" onPress={onCheckInPressed} icon= {<FontAwesome5 name={'camera-retro'} color="#fff" solid size={30} />} />
                <CustomButtonIcon text="Check Out" onPress={onCheckOutPressed} icon= {<FontAwesome5 name={'camera-retro'} color="#fff" solid size={30} />}/>
                <CustomButtonIcon text="Add New" onPress={onAddNewPressed} icon= {<FontAwesome5 name={'camera-retro'} color="#fff" solid size={30} />}/>
            </View>
        </View>

        
    );
};

const styles = StyleSheet.create({

    button: {
        padding: 80,
        marginTop: '-45%'
    },

    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center'   
    },

    avatar: {
        position: 'absolute',
        top: 90,
        
        alignItems: 'center',
        width: '40%',
        height: '25%',
        borderColor: '#fff',
        borderWidth: 4,
        borderRadius: 1000,
    },
});

export default SelectCam