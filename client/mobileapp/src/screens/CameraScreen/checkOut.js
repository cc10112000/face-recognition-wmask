import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Alert, Modal, ScrollView, Dimensions
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import { useCamera } from 'react-native-camera-hooks';
import CustomButton from '../../components/CustomButton/CustomButton';
import axios from 'axios';
import useToken from "../SignInScreen/useToken";
import jwt_decode from "jwt-decode";

export default function CheckOut({ navigation, route }) {

    const [{ cameraRef }, { takePicture }] = useCamera(null);
    const { token } = useToken();


    const checkOut = async () => {
        const options = {
            quality: 0.5,
            base64: true
        };
        const decoded = await jwt_decode(token['_W']);
        const data = await takePicture(options);

        await axios.post("http://10.0.2.2:5000/check", { base64: data.base64, devices: "mobile", token: decoded.sub, check_status: "out", room_id: "NULL" })
            .then(function (response) {
                Alert.alert(" " + response.data.user_name + " " + response.data.mask_status + " " + response.data.datetime);
                console.log(response.data);
            })
            .catch(function (error) {
                console.log(error.response.error);
            });

    }


    return (
        <View style={styles.body}>
            <RNCamera style={styles.preview} type={RNCamera.Constants.Type.front} ref={cameraRef}>
            </RNCamera>
            <View style={{ width: '85%' }}>
                <CustomButton text="Check Out" onPress={checkOut} />
            </View>
        </View>
    );
}



const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    preview: {

        flex: 1,
        alignItems: 'center',
        padding: 10,
        marginVertical: 5,
    },
    text: {
        fontSize: 18,
        color: 'white',
    },
    backText: {
        textAlignVertical: 'center',
        color: '#2CBDFB',
        fontSize: 15
    },

    headline: {
        width: '100%',
        flexDirection: 'row',
    },

    title: {
        textAlignVertical: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#2CBDFB',
        margin: 10,
        marginBottom: 20,
        marginLeft: 80,
        marginTop: 15
    },
    selectBox: {
        flexDirection: 'row',
        backgroundColor: '#F6F6F6',
        width: '85%',
        height: '10%',
        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 10,
        fontSize: 15,
        alignItems: 'center',
        paddingHorizontal: 10,
        marginVertical: 5,

    },
    modal: {
        backgroundColor: 'white',
        borderRadius: 10
    },
    option: {
        alignItems: 'flex-start'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

