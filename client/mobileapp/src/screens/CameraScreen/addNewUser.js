import React, { useState, useEffect, useRef } from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Alert, Modal, ScrollView, Dimensions
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import { useCamera } from 'react-native-camera-hooks';
import CustomInput from '../../components/CustomInput/CustomInput';
import CustomButton from '../../components/CustomButton/CustomButton';
import axios from 'axios';
import useToken from "../SignInScreen/useToken";
import jwt_decode from "jwt-decode";
import SweetAlert from 'react-native-sweet-alert';

export default function AddNewUser({ navigation, route }) {

    const [{ cameraRef }, { takePicture }] = useCamera();
    // const cameraRef = useRef(null);
    const [username, setUsername] = useState('');
    const [userid, setUserid] = useState('');

    const onCancelPressed = () => {
        console.warn("Cancel")
        navigation.navigate('SelectCamera')

    }

    const checkAlert = (title, text, icon) => {
        SweetAlert.showAlertWithOptions({
            title: title,
            text: text,
            icon: icon,
            timer: 2000,
        });
    };

    const loadingImg = async () => {
        const options = { width: 500, height: 500, base64: true };
        let dataTrain = await takePicture(options);
        let listCap = [];
        const myInterval = setInterval(() => {
            listCap.push(dataTrain.base64);
        }, 500);

        setTimeout(() => {
            clearInterval(myInterval);
            // console.log(listCap.length)
            // console.log(userid)
            // console.log(username)
            axios.post("http://10.0.2.2:5000/trainfeature", {
                base64: listCap,
                user_id: userid,
                user_name: username,
                device: "mobile"
            })
                .then((res) => {
                    checkAlert("Add successfully!", "New user face is embedded in model", "success")
                    console.log(res.data)
                })
                .catch((error) => {
                    checkAlert("Unexpected error", "May be user id is duplicated, Try again!", "error")
                    console.log(error);
                });
        }, 5300);

    }

    return (
        <View style={styles.body}>
            <RNCamera style={styles.preview} type={RNCamera.Constants.Type.front} ref={cameraRef}>
            </RNCamera>
            <CustomInput
                placeholder="Id"
                value={userid}
                setValue={setUserid}
            />
            <CustomInput
                placeholder="Name"
                value={username}
                setValue={setUsername}
            />
            <View style={{ marginTop: 20, width: '100%' }}>
                <CustomButton text="Add User" onPress={loadingImg} />
                <CustomButton text="Cancel" onPress={onCancelPressed} />
            </View>
        </View>
    );
}



const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    preview: {

        flex: 1,
        alignItems: 'center',
        padding: 10,
        marginVertical: 5,
    },
    text: {
        fontSize: 18,
        color: 'white',
    },
    backText: {
        textAlignVertical: 'center',
        color: '#2CBDFB',
        fontSize: 15
    },

    headline: {
        width: '100%',
        flexDirection: 'row',
    },

    title: {
        textAlignVertical: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#2CBDFB',
        margin: 10,
        marginBottom: 20,
        marginLeft: 80,
        marginTop: 15
    },
    selectBox: {
        flexDirection: 'row',
        backgroundColor: '#F6F6F6',
        width: '85%',
        height: '10%',
        borderColor: '#E8E8E8',
        borderWidth: 2,
        borderRadius: 10,
        fontSize: 15,
        alignItems: 'center',
        paddingHorizontal: 10,
        marginVertical: 5,

    },
    modal: {
        backgroundColor: 'white',
        borderRadius: 10
    },
    option: {
        alignItems: 'flex-start'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

