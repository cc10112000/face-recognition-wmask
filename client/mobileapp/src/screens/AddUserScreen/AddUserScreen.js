import React, { useState } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import CustomInput from '../../components/CustomInput/CustomInput';
import CustomButton from '../../components/CustomButton/CustomButton';
import { useNavigation } from '@react-navigation/native';

const AddUserScreen = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [birthday, setBirthday] = useState('');
  const [sex, setSex] = useState('');
  const navigation =useNavigation();
  const onCompletePressed = () => {
    console.warn("Complete")
  }
  const onCancelPressed = () => {
    console.warn("Cancel")
    navigation.navigate('AdminScreen')

  }

  return (
    <View style={styles.root}>
      <Text style={styles.title}>Add User Information</Text>

      <CustomInput
        placeholder="Id"
        value={username}
        setValue={setUsername}
      />
      <CustomInput
        placeholder="Name"
        value={sex}
        setValue={setSex}
      />
      <View style={{marginTop: 20, width: '100%'}}>
        <CustomButton text="Scan" onPress={onCompletePressed} />
        <CustomButton text="Cancel" onPress={onCancelPressed} />
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    marginLeft: 50,
    width: '70%',
    maxWidth: 300,
    maxHeight: 200,
    borderRadius: 20,
  },

  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#2CBDFB',
    margin: 10,
    marginBottom: 30,
  },

});

export default AddUserScreen;
