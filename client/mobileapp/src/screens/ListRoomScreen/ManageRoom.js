import {
    View, Text, StyleSheet, Dimensions,
    LogBox
} from 'react-native'
import React from 'react'
import { CustomSwitchRoom } from '../../components/CustomButton';
import { useNavigation } from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
LogBox.ignoreAllLogs();

const ManageRoom = () => {
    const navigation = useNavigation();
    const onSelectSwitch = () => {
        console.warn("Switch")
    }
    const onBackPressed = () => {
        console.warn("Back")
        navigation.navigate('ListRoom')
    }


    const { header, container, button } = styles;
    return (
        <View style={container}>
            <View style={header}>
                <View style={styles.headline}>
                    <FontAwesome5 style={styles.back} name={'angle-left'} color='#fff' solid size={35} onPress={onBackPressed} />
                    <Text style={styles.title}>Manage Room</Text>
                </View>
                <View style={styles.detailsRoom}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.fontId, { flex: 3.5 }]} >ID Room:</Text>
                        <Text style={[styles.fontId, { flex: 4 }]} >Quantity:</Text>
                    </View>
                    <View>
                        <Text style={styles.fontId}>Creator: </Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {/* <Text style={[styles.fontId, { flex: 3.5 }]}>Start:</Text>
                        <Text style={[styles.fontId, { flex: 4 }]}>Time:</Text> */}
                    </View>
                </View>
                <View style={button}>
                    <CustomSwitchRoom
                        selectionMode={1}
                        roundCorner={true}
                        option1={'Attendance'}
                        option2={'Settings'}
                        onSelectSwitch={onSelectSwitch}
                        selectionColor={'#2CBDFB'}
                    />
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        padding: 20,
        marginTop: 60
    },

    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

    headline: {
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: '5%',
        width: '100%',
        justifyContent: 'center'
    },
    back: {
        textAlignVertical: 'center',
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: '20%',
        marginRight: '20%',
    },
    header: {
        height: 240,
        backgroundColor: '#2CBDFB',
        alignItems: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    avatar: {
        position: 'absolute',
        top: 90,
        left: (Dimensions.get('window').width / 2) - 90,
        alignItems: 'center',
        width: 180,
        height: 180,
        borderColor: '#fff',
        borderWidth: 4,
        borderRadius: 100,
    },

    meInfor: {
        alignItems: 'center',
        top: 50,
        width: (Dimensions.get('window').width),
    },
    detailsRoom: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        backgroundColor: 'white',
        width: '85%',
        height: 80,
        borderRadius: 10,
        marginTop: 40
    },
    fontId: {
        fontWeight: '900',
        fontSize: 14,
        margin: 2
    }
});

export default ManageRoom