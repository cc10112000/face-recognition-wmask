import React, { useState } from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';
import ListRoom from '../../components/CustomList/ListRoom';
import { useNavigation } from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';



const ListRoomScreen = () => {

  
  const [search, setSearch] = useState('');

  const navigation = useNavigation();

  const onBackPressed = () => {
    console.warn("Manage User")
    navigation.navigate('AdminScreen')
  }

  const onCreatePressed = () => {
    console.warn("Manage User")
    navigation.navigate('CreateRoom')
  }

  return (
    <SafeAreaView>
      <View style={styles.root}>
        <View style={styles.headline}>
          <FontAwesome5 style={styles.back} name={'angle-left'} color='#2CBDFB' solid size={35} onPress={onBackPressed}/>
          <Text style={styles.title} >List Room</Text>
          <FontAwesome5 style={styles.createRoom} name={'plus-square'} color='#2CBDFB' solid size={35} onPress={onCreatePressed}/>
        </View>
        <ListRoom />
      </View>
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  back: {
    textAlignVertical: 'center',
  },
  createRoom:{
    textAlignVertical: 'center',
  },

  headline: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: '5%',
    width: '100%',
    justifyContent: 'center'
  },

  title: {
    textAlignVertical: 'center',
    alignItems:'center',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#2CBDFB',
    marginLeft: '25%',
    marginRight: '25%'
  },

});

export default ListRoomScreen;
