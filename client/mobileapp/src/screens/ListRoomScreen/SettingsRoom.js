import { Text, View, StyleSheet } from 'react-native'
import React, { useState, useEffect } from 'react';
import CustomButton from '../../components/CustomButton/CustomButton';

const SettingsRoom = () => {

  const onUpdatePressed = () => {
    console.warn("Update Room")
  }
  const onDeletePressed = () => {
    console.warn("Delete Room")
  }

  return (
    <View style={styles.button}>
      <CustomButton text="Update Room" onPress={onUpdatePressed} />
      <CustomButton text="Delete Room" onPress={onDeletePressed} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  button: {
    alignItems: 'center',
    padding: 30,
  },
});

export default SettingsRoom