import { View, StyleSheet, SafeAreaView, Text } from 'react-native';
import { CustomButton } from '../../components/CustomButton';
import React from 'react';
import { useNavigation } from '@react-navigation/native';


const CheckLog = () => {
  const navigation = useNavigation();
  const onLogCheckPressed = () => {
    navigation.navigate('ListUser')
  }
  return (
    <SafeAreaView >
      <View style={styles.root}>
        <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
          <Text style={styles.fontText}>Number of attend:</Text>
          <Text style={[styles.fontText, {color: 'green'}]}>800</Text>
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
          <Text style={styles.fontText}>Number of absent:</Text>
          <Text style={[styles.fontText, {color: 'red'}]}>200</Text>
        </View>
      </View>

      <View style={styles.button}>
        <CustomButton text="Check Log" onPress={onLogCheckPressed} />
      </View>
    </SafeAreaView>
  )
};

const styles = StyleSheet.create({
  root: {
    marginTop: 40,
    alignItems: 'center',
  },
  fontText: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: 5
  },
  button: {
    alignItems: 'center',
    padding: 30,
  },
});


export default CheckLog