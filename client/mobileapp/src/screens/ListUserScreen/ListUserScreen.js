import React from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';
import CustomList from '../../components/CustomList/CustomList';
import { useNavigation } from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const ListUserScreen = () => {

  const navigation = useNavigation();

  const onBackPressed = () => {
    console.warn("Manage User")
    navigation.navigate('AdminScreen')
  }

  return (
    <SafeAreaView>
      <View style={styles.root}>
        <View style={styles.headline}>
          <FontAwesome5 style={styles.back} name={'angle-left'} color='#2CBDFB' solid size={35} onPress={onBackPressed} />
          <Text style={styles.title} >Check Log</Text>
        </View>
        <CustomList />
      </View>
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },

  back: {
    alignItems:'flex-start' ,
    textAlignVertical: 'center',
  },

  headline: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: '5%',
    width: '100%',
    justifyContent: 'center'
  },

  title: {
    textAlignVertical: 'center',
    alignItems: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#2CBDFB',
    marginLeft: '28%',
    marginRight: '28%'
  },

});

export default ListUserScreen;
