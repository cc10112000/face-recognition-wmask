import { Text, View, StyleSheet } from 'react-native'
import React, { useState, useEffect } from 'react';
import CustomButton from '../../components/CustomButton/CustomButton';

const SettingsUser = () => {

  const onUpdatePressed = () => {
    console.warn("Update User")
  }
  const onDeletePressed = () => {
    console.warn("Delete User")
  }

  return (
    <View style={styles.button}>
      <CustomButton text="Update User" onPress={onUpdatePressed} />
      <CustomButton text="Delete User" onPress={onDeletePressed} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  button: {
    alignItems: 'center',
    padding: 30,
  },
});

export default SettingsUser