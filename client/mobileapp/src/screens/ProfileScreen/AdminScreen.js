import { View, Text, StyleSheet, Image, Dimensions } from 'react-native'
import Avatar from '../../../assets/image/Admin1.png';
import { CustomButtonIcon } from '../../components/CustomButton';
import Header from '../../components/Header';
import React from 'react'
import { useNavigation } from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


const AdminScreen = () => {

    const navigation =useNavigation();

    const onCreatePressed = () => {
        console.warn("Create Pressed")
        navigation.navigate('CreateRoom')
    }

    const onListUserPressed = () => {
        console.warn("Manage User")
        navigation.navigate('ListUser')
    }

    const onAddPressed = () => {
        console.warn("Add User")
        navigation.navigate('AddUser')
    }

    const onListRoomPressed = () => {
        console.warn("Manage Room")
        navigation.navigate('ListRoom')
    }

    const onListBookingPressed = () => {
        console.warn("Manage Booking")
        navigation.navigate('ListBooking')
    }
    
    const { avatar, container, button } = styles;
    return (
        <View style={container}>
            <Header />
            <Image
                style={avatar}
                source={Avatar}
            />
            <View style={button}>
                <CustomButtonIcon text="Check Log List" onPress={onListUserPressed} icon= {<FontAwesome5 name={'list'} color='white' solid size={30}/>}/>
                <CustomButtonIcon text="List Room" onPress={onListRoomPressed} icon= {<FontAwesome5 name={'window-maximize'} color='white' solid size={30}/>}/>
                <CustomButtonIcon text="List Booking Room" onPress={onListBookingPressed} icon= {<FontAwesome5 name={'calendar-plus'} color='white' solid size={30}/>}/>
            </View>
        </View>

        
    );
};

const styles = StyleSheet.create({

    button: {
        padding: 80,
        marginTop: '-45%'
    },

    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center'   
    },

    avatar: {
        position: 'absolute',
        top: 90,
        
        alignItems: 'center',
        width: '40%',
        height: '25%',
        borderColor: '#fff',
        borderWidth: 4,
        borderRadius: 1000,
    },
});

export default AdminScreen