import {
    View, Text, StyleSheet, Image, Dimensions,
    LogBox
} from 'react-native'
import Avatar from '../../../assets/image/User.png';
import React from 'react'
import { useNavigation } from '@react-navigation/native';
import InformationUser from './InformationUser';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

LogBox.ignoreAllLogs();

const UserScreen = () => {
    const navigation = useNavigation();
    const onSelectSwitch = () => {
        console.warn("Switch")
    }
    const onBackPressed = () => {
        console.warn("Back")
        navigation.navigate('ListUser')
    }


    const { header, avatar, container, button } = styles;
    return (
        <View style={container}>
            <View style={header}>
                <View style={styles.headline}>
                    <FontAwesome5 style={styles.back} name={'angle-left'} color='#fff' solid size={35} onPress={onBackPressed} />
                    <Text style={styles.title}>User Profile</Text>
                </View>
            </View>
            <Image
                style={avatar}
                source={Avatar}
            />
            <View style={button}>
                <InformationUser />
            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    // header: {
    //     flex: 1,
    //     backgroundColor: "#2CBDFB",
    //     alignItems: 'center',

    // },

    // footer: {
    //     flex: 2, backgroundColor: "#fff"
    // },
    button: {
        width: '100%',
        // alignItems: 'center',
        padding: 20,
        marginTop: 60
    },

    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center'
    },

    headline: {
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: '5%',
        width: '100%',
        justifyContent: 'center'
    },

    back: {
        alignItems: 'flex-start',
        textAlignVertical: 'center',
    },

    title: {
        textAlignVertical: 'center',
        alignItems: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: '25%',
        marginRight: '25%'
    },
    header: {
        height: '36%',
        backgroundColor: '#2CBDFB',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        width: '95%',
        marginTop: '2%'
    },
    avatar: {
        position: 'absolute',
        top: 90,
        left: (Dimensions.get('window').width / 2) - 90,
        alignItems: 'center',
        width: 180,
        height: 180,
        borderColor: '#fff',
        borderWidth: 4,
        borderRadius: 100,
    },

    meInfor: {
        alignItems: 'center',
        top: 50,
        width: (Dimensions.get('window').width),
    },
});

export default UserScreen