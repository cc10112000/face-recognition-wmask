import { View, Text, Image, StyleSheet, SafeAreaView, ActivityIndicator, TextInput, StatusBar, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import { useRoute } from '@react-navigation/native';
import { useTheme } from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';

const InformationUser = () => {
  const route = useRoute();
  const [data, setData] = useState([]);
  const { user_id, user_name } = route.params;
  const { colors } = useTheme();

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#2CBDFB' barStyle="light-content" />
      <Animatable.View
        animation="fadeInUpBig"
        style={[styles.footer, {
          backgroundColor: colors.background
        }]}
      >
        <Text style={[styles.text_footer, {
          color: colors.text
        }]}>User Id</Text>
        <View style={styles.action}>
          <FontAwesome
            name="id-badge"
            color={colors.text}
            size={25}
          />
          <TextInput
            editable={false}
            selectTextOnFocus={false}
            placeholderTextColor="#666666"
            style={[styles.textInput, {
              color: colors.text
            }]}
            autoCapitalize="none"
          >{route.params.user_id}</TextInput>
        </View>

        <Text style={[styles.text_footer, {
          color: colors.text
        }]}>User Name</Text>
        <View style={styles.action}>
          <FontAwesome
            name="user"
            color={colors.text}
            size={25}
          />
          <TextInput
            editable={false}
            selectTextOnFocus={false}
            placeholder="Quantity"
            placeholderTextColor="#666666"
            style={[styles.textInput, {
              color: colors.text
            }]}
            autoCapitalize="none"
          >{route.params.user_name}</TextInput>
        </View>

      </Animatable.View>
    </View>
  )
};

const styles = StyleSheet.create({
  // container: {
  //   backgroundColor: 'white',
  //   alignItems: 'flex-start',
  //   justifyContent: 'center',
  //   padding: 24,
  // },
  // paragraphStyle: {
  //   paddingTop: 5,
  //   paddingBottom: 20,
  //   fontSize: 14,
  //   alignItems: 'flex-start',
  //   borderBottomWidth: 0.5,
  //   width: '100%'
  // },
  logoStyle: {
    height: 50,
    width: 50,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    fontSize: 20
  },
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingBottom: 10
  },
  footer: {
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderBottomEndRadius: 30,
    borderBottomStartRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
});


export default InformationUser