import React, { useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    StatusBar,
    Image,
    ScrollView,
    SafeAreaView,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useTheme } from '@react-navigation/native';
import Swiper from 'react-native-swiper'
const images = [
    'https://weseenow.co.uk/wp-content/uploads/2019/03/Facial-recognition-software.jpg',
    'https://www.sodapdf.com/blog/wp-content/uploads/2020/01/Facial-recognition-thumbnail-768x403.jpg',
    'https://www.aindralabs.com/wp-content/uploads/World-water-day-1.png',
    'https://blog.dormakaba.com/tachyon/2020/04/dormakaba-Blog-Post-pictures-_-1024-x-683-83.jpg',
]

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const SplashScreen = ({ navigation }) => {
    const { colors } = useTheme();
    const [imgActivate, setimgActivate] = useState(0);

    onchange = (nativeEvent) => {
        if (nativeEvent) {
            const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
            if (slide != imgActivate) {
                setimgActivate(slide);
            }
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#2CBDFB' barStyle="light-content" />
            <SafeAreaView style={styles.wrap}>
                <View style={styles.wrap}>
                    <View style={styles.sliderContainer}>
                        <Swiper autoplay horizontal={false} height={200} activeDotColor="#2CBDFB">
                            <View style={styles.slide}>
                                <Image
                                    source={require('../../assets/image/test.jpg')}
                                    resizeMode='cover'
                                    style={styles.sliderImage}
                                />
                            </View>
                            <View style={styles.slide}>
                                <Image
                                    source={require('../../assets/image/test1.jpg')}
                                    resizeMode='cover'
                                    style={styles.sliderImage}
                                />
                            </View>
                            <View style={styles.slide}>
                                <Image
                                    source={require('../../assets/image/test2.jpg')}
                                    resizeMode='cover'
                                    style={styles.sliderImage}
                                />
                            </View>
                        </Swiper>
                    </View>
                </View>
            </SafeAreaView >

            <Animatable.View
                style={[styles.footer, {
                    backgroundColor: colors.background
                }]}
                animation="fadeInUpBig"
            >
                <Text style={[styles.title, {
                    color: colors.text
                }]}>Face Recognition Mask</Text>
                <Text style={styles.text}>Sign in with account</Text>
                <View style={styles.button}>
                    <TouchableOpacity onPress={() => navigation.navigate('SignInScreen')}>
                        <LinearGradient
                            colors={['#2CBDFB', '#27aae1']}
                            style={styles.signIn}
                        >
                            <Text style={styles.textSign}>Get Started</Text>
                            <MaterialIcons
                                name="navigate-next"
                                color="#fff"
                                size={20}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        </View >
    );
};

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2CBDFB'
    },
    wrap: {
        padding: 2,
        flex: 2,
    },
    wrapDot: {
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    dotActive: {
        margin: 3,
        color: 'black'
    },
    dot: {
        margin: 3,
        color: '#888'
    },
    footer: {
        flex: 1,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30
    },
    title: {
        color: '#2CBDFB',
        fontSize: 30,
        fontWeight: 'bold'
    },
    text: {
        color: 'grey',
        marginTop: 5
    },
    button: {
        alignItems: 'flex-end',
        marginTop: '20%'
    },
    signIn: {
        width: 150,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        flexDirection: 'row'
    },
    textSign: {
        color: 'white',
        fontWeight: 'bold'
    },
    sliderContainer: {
        height: 200,
        width: '90%',
        marginTop: '20%',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 0,
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderRadius: 8,
    },
    sliderImage: {
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
    }
});

