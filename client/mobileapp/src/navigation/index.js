import { View, ActivityIndicator } from 'react-native'
import React, { useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AddUserScreen from '../screens/AddUserScreen';
import ListUserScreen from '../screens/ListUserScreen/ListUserScreen';
import AdminScreen from '../screens/ProfileScreen/AdminScreen';
import CreateRoomScreen from '../screens/CreateRoomScreen';
import UserScreen from '../screens/ProfileScreen/UserScreen';
import { ManageRoom, ListRoomScreen } from '../screens/ListRoomScreen';
import { AuthContext } from '../components/Context';
import 'react-native-gesture-handler';
import RootStackScreen from '../screens/RootStackScreen';
import useToken from '../screens/SignInScreen/useToken';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ListBookingRoomScreen from '../screens/ListBookingRoom/ListBookingRoomScreen';
import CheckOut from '../screens/CameraScreen/checkOut';
import CheckIn from '../screens/CameraScreen/camScreen';
import AddNew from '../screens/CameraScreen/addNewUser';
import SelectCamera from '../screens/CameraScreen/selectCam';
const Stack = createStackNavigator();

const Navigation = () => {
  const [userToken, setUserToken] = React.useState(null); 
  const { token, removeToken, setToken } = useToken();
  const initialLoginState = {
    isLoading: true,
    userToken: null,
  };

  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGIN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGOUT':
        return {
          ...prevState,
          userToken: null,
          isLoading: false,
        };
    }
  };

  const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState);

  const authContext = React.useMemo(() => ({
    signIn: async (access_token) => {
      const userToken = String(access_token);    
      dispatch({ type: 'LOGIN', token: userToken });
      console.log(userToken)
    },
    signOut: async () => {
      try {
        await AsyncStorage.removeItem('token');
      } catch(e) {
        console.log(e);
      }
      dispatch({ type: 'LOGOUT' });
      console.log(userToken)
    },
  }));

  useEffect(() => {
    setTimeout(async() => {
      let userToken = null;
      try {
        userToken = await AsyncStorage.getItem('token');
      } catch(e) {
        console.log(e);
      }
      dispatch({ type: 'RETRIEVE_TOKEN', token: userToken });
      console.log(userToken)
    }, 1000)
  }, []);

  if (loginState.isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" />
      </View>
    )
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {loginState.userToken !== null ? (
          <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="AdminScreen" component={AdminScreen} />
            <Stack.Screen name="ListUser" component={ListUserScreen} />
            <Stack.Screen name="ManageUser" component={UserScreen} />
            <Stack.Screen name="AddUser" component={AddUserScreen} />
            <Stack.Screen name="CreateRoom" component={CreateRoomScreen} />
            <Stack.Screen name="ListRoom" component={ListRoomScreen} />
            <Stack.Screen name="ManageRoom" component={ManageRoom} />
            <Stack.Screen name="ListBooking" component={ListBookingRoomScreen} />
            <Stack.Screen name = "CheckOut" component = {CheckOut}/>
            <Stack.Screen name = "CheckIn" component = {CheckIn}/>
            <Stack.Screen name = "AddNew" component = {AddNew}/>
            <Stack.Screen name = "SelectCamera" component = {SelectCamera}/>
          </Stack.Navigator>
        ) 
          :
          <RootStackScreen />
        }
      </NavigationContainer>
    </AuthContext.Provider>
  )
}

export default Navigation