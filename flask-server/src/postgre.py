import psycopg2
import psycopg2.extras


hostname = 'localhost'
database = 'faceRecognitionv2'
username = 'postgres'
pwd = '123a123'
port_id = 5432
conn = None
cur = None


def connection():
    return psycopg2.connect(
        host=hostname,
        dbname=database,
        user=username,
        password=pwd,
        port=port_id
    )


def insert_user(msv, name):
    try:
        conn = connection()
        cur = conn.cursor()
        insert_script = 'INSERT INTO users (user_id ,user_name) VALUES (%s,%s)'
        insert_value = (msv, name)
        cur.execute(insert_script, insert_value)
        conn.commit()
    except Exception as error:
        if error is not None:
            print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()

def insert_room(name, capacity,description):
    insert_rows = 0;
    try:
        conn = connection()
        cur = conn.cursor()
        insert_script = 'INSERT INTO room (room_name, capacity, description) VALUES (%s,%s,%s)'
        insert_value = (name, str(capacity), description)
        cur.execute(insert_script, insert_value)
        insert_rows = cur.rowcount
        conn.commit()
    except Exception as error:
        if error is not None:
            print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
    return insert_rows       

def get_all_room():
    datas = []
    try:
        conn = connection()
        cur = conn.cursor()
        insert_script = 'SELECT * FROM room'
        cur.execute(insert_script)
        rows = cur.fetchall() 
            
        for row in rows:
            des = 'No Description' if row[3] is None else row[3]  
            data = {
            'id':row[0],
            'name':row[1],
            'capacity': row[2],
            'description': des
        }
            datas.append(data)
        conn.commit()
    except Exception as error:
        if error is not None:
            print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
    return datas        

def update_room(name, capacity,desciption,id):
    updated_rows = 0
    try:
        conn = connection()
        cur = conn.cursor()
        update_script = """ UPDATE room
                SET room_name = %s,
                    capacity = %s,
                    description = %s
                WHERE room_id = %s"""
        insert_value = (name, capacity,desciption,id)
        cur.execute(update_script, insert_value)
        updated_rows = cur.rowcount
        conn.commit()
        print(updated_rows)
    except Exception as error:
        if error is not None:
            print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
        return updated_rows


def delete_room(id):
    rows_deleted = 0
    try:
        conn = connection()
        cur = conn.cursor()
        delete_script = "DELETE FROM room WHERE room_id ="+str(id)
        cur.execute(delete_script)
        rows_deleted = cur.rowcount
        conn.commit()
        
    except Exception as error:
        if error is not None:
            print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
    return rows_deleted

def find_user_by_id(id):
    try:
        conn = connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT user_name FROM users where user_id = '"+id+"'")

        rows = cur.fetchall()
        if rows is not None:
            return rows[0]['user_name']
    except Exception as error:
        print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
    return None

def insert_booking_room(room_id, admin_id, date, session):
    rows_count = 0
    try:
        conn = connection()
        cur = conn.cursor()
        insert_script = 'INSERT INTO booking (room_id ,admin_id, booking_date, booking_session) VALUES (%s,%s,%s,%s)'
        print(room_id)
        print(admin_id)
        print(date)
        print(session)
        insert_value = (int(room_id), int(admin_id), date, session)
        cur.execute(insert_script, insert_value)
        rows_count = cur.rowcount()
        conn.commit()
    except Exception as error:
        if error is not None:
            print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
    return rows_count

def find_user_by_id2(id):
    try:
        conn = connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM users where user_id = '"+id+"'")
        
        rows = cur.fetchall()
        return rows[0]
    except Exception as error:
        print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:   
            conn.close()
    return None



def insert_check(user_id, admin_id, room_id, check_status, note, mask_check):
    try:
        conn = connection()
        cur = conn.cursor()
        insert_script = 'INSERT INTO checklog (user_id ,admin_id,room_id, status,note,check_mask) VALUES (%s,%s,%s,%s,%s,%s)'
        insert_value = (user_id, admin_id, room_id, check_status, note, mask_check)
        cur.execute(insert_script, insert_value,)
        conn.commit()
    except Exception as error:
        print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()


def find_admin_by_id(acc):
    try:
        conn = connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM adminaccounts where admin_acc = '"+acc+"'")

        rows = cur.fetchall()
        if rows is not None:
            return rows[0]

    except Exception as error:
        print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()


def find_admin_acc(id):
    try:
        conn = connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM adminaccounts where admin_id="+str(id))

        rows = cur.fetchall()
        if rows is not None:
            return rows

    except Exception as error:
        print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()


def find_all_log():
    try:
        conn = connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM checklog")
        rows = cur.fetchall()

        for row in rows:

            row[2] = find_admin_acc(row[2])[0][1]
            row[3] = 'None' if row[3] is None else find_room_by_id(row[3])[0][1]
            row[5] = 'Masked'if row[5] else 'No Mask'
            row[6] = str(row[6].strftime("%H:%M:%S"))
            row[7] = str(row[7])
            row[8] = 'No description' if row[8] is None else row[8]
        return rows

    except Exception as error:
        print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
    return None

def find_room_by_id(id):
    try:
        conn = connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM room where room_id="+str(id))

        rows = cur.fetchall()
        if rows is not None:
            return rows

    except Exception as error:
        print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()

def find_all_booking():
    datas = []
    try:
        conn = connection()
        cur = conn.cursor()
        insert_script = 'SELECT * FROM booking'
        cur.execute(insert_script)
        rows = cur.fetchall()       
        for row in rows:
            data = {
            'room': find_room_by_id(row[0])[0][1],
            'admin_name':find_admin_acc(row[1])[0][1],
            'booking_date': str(row[2]),
            'booking_session':row[3],
            'execution_time': str(row[4].strftime("%m/%d/%Y, %H:%M:%S"))
        }
            datas.append(data)
        conn.commit()
    except Exception as error:
        if error is not None:
            print(error)
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()
    return datas


if __name__ == "__main__":
    rows = find_all_booking()
    for row in rows:
        print(row)

