
from asyncio.windows_events import NULL
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import numpy as np
import argparse
import pickle
import time
import cv2
import os
from CreateClassifier import CreateClassifier

print("[INFO] loading face detector model...")
prototxtPath = '../gui/face_detector/deploy.prototxt'
weightsPath = '../gui/face_detector/res10_300x300_ssd_iter_140000.caffemodel'
faceNet = cv2.dnn.readNet(prototxtPath, weightsPath)

# load the face mask detector model from disk
print("[INFO] loading face mask detector model...")
maskNet = load_model('../gui/mask_detector.model')


def detect_and_predict_mask(frame, faceNet, maskNet):
    # grab the dimensions of the frame and then construct a blob
    # from it
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(frame, 1.0, (300, 300),
                                 (104.0, 177.0, 123.0))

    # pass the blob through the network and obtain the face detections
    faceNet.setInput(blob)
    detections = faceNet.forward()

    # initialize our list of faces, their corresponding locations,
    # and the list of predictions from our face mask network
    faces = []
    locs = []
    preds = []

    # loop over the detections
    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with
        # the detection
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the confidence is
        # greater than the minimum confidence
        if confidence > 0.5:
            # compute the (x, y)-coordinates of the bounding box for
            # the object
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # ensure the bounding boxes fall within the dimensions of
            # the frame
            (startX, startY) = (max(0, startX), max(0, startY))
            (endX, endY) = (min(w - 1, endX), min(h - 1, endY))

            # extract the face ROI, convert it from BGR to RGB channel
            # ordering, resize it to 224x224, and preprocess it
            face = frame[startY:endY, startX:endX]
            if face.any():
                face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
                face = cv2.resize(face, (224, 224))
                face = img_to_array(face)
                face = preprocess_input(face)

                # add the face and bounding boxes to their respective
                # lists
                faces.append(face)
                locs.append((startX, startY, endX, endY))

    # only make a predictions if at least one face was detected
    if len(faces) > 0:
        # for faster inference we'll make batch predictions on *all*
        # faces at the same time rather than one-by-one predictions
        # in the above `for` loop
        faces = np.array(faces, dtype="float32")
        preds = maskNet.predict(faces, batch_size=32)

    # return a 2-tuple of the face locations and their corresponding
    # locations
    return (locs, preds)


def faceRecByImage(img):
    recognizer = CreateClassifier(embedding_path='outputs/finalmodelv1.pickle')
    embed_out = recognizer.face_embedding.embed_face(img)

    (locs, preds) = detect_and_predict_mask(img, faceNet, maskNet)
    for (box, pred) in zip(locs, preds):
        (mask, withoutMask) = pred
        mask_percent = max(mask, withoutMask)

    if not embed_out is None:
        bbox, embedding = embed_out
        x, y, w, h = map(int, bbox)
        model = recognizer.model
        proba = model.predict_proba([embedding])[0]
        id = np.argmax(proba)
        name = recognizer.labels_unique[id]
        confidence = proba[id]
        if mask < withoutMask and confidence >= 0.8:
                label = "No Mask"
                img = cv2.rectangle(img, (x, y), (w, h), (0, 0, 255), 2)
                return (name, label, img)
        elif mask < withoutMask and confidence < 0.8:
                label = "No Mask"
                name = "unknown"
                img = cv2.rectangle(img, (x, y), (w, h), (0, 0, 255), 2)
                return (name, label, img)
        elif mask > withoutMask and confidence >= 0.6:
                label = "Masked"
                img = cv2.rectangle(img, (x, y), (w, h), (0, 0, 255), 2)
                return (name, label, img)
        else:
                label = "Masked"
                name = "unknown"
                img = cv2.rectangle(img, (x, y), (w, h), (0, 0, 255), 2)
                return (name, label, img)


# if __name__ == "__main__":
#     name, label = faceRecByImage(r'C:\Users\cc101\Pictures\Camera Roll\WIN_20220326_15_39_56_Pro.jpg')
#     print(name)
#     print(label)
