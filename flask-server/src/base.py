import json
from flask import Flask, request, jsonify
from flask_cors import CORS
from datetime import datetime, timedelta, timezone
from flask_jwt_extended import create_access_token,get_jwt,get_jwt_identity, \
                               unset_jwt_cookies, jwt_required, JWTManager
import postgre as sql

app = Flask(__name__)
CORS(app)
app.config["JWT_SECRET_KEY"] = "duongnek19"
jwt = JWTManager(app)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=1)


@app.after_request
def refresh_expiring_jwts(response):
    try:
        exp_timestamp = get_jwt()["exp"]
        now = datetime.now(timezone.utc)
        target_timestamp = datetime.timestamp(now + timedelta(minutes=30))
        if target_timestamp > exp_timestamp:
            access_token = create_access_token(identity=get_jwt_identity())
            data = response.get_json()
            if type(data) is dict:
                data["access_token"] = access_token 
                response.data = json.dumps(data)
        return response
    except (RuntimeError, KeyError):
        # Case where there is not a valid JWT. Just return the original respone
        return response


@app.route('/login', methods=["POST"])
def create_token():
    admin_acc = request.json.get("admin_acc", None)
    password = request.json.get("password", None)
    check = sql.find_user_by_id_admin(admin_acc)
    print(admin_acc)
    print(password)
    print(check) 
    if check is None or (admin_acc != check[1] or password != check[2])  :
        return {"msg": "Wrong email or password"}, 401
    access_token = create_access_token(identity=admin_acc)
    response = {"access_token":access_token}
    print(response)
    return response
    


@app.route('/profile')
@jwt_required() 
def my_profile():
    response_body = {
        "name": "Duong",
        "about" :"That bai roi moi nguoi oi"
    }

    return response_body 

@app.route("/logout", methods=["POST"])
def logout():
    response = jsonify({"msg": "logout successful"})
    unset_jwt_cookies(response)
    return response


if __name__ == '__main__':
    app.run()