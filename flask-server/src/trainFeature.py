import os
import glob
from CreateClassifier import CreateClassifier




def train_classifer(id):
    recognizer = CreateClassifier(embedding_path='outputs/finalmodelv1.pickle')
    data_dir = os.path.join(os.getcwd(), 'trainFuturetmp/*/*.jpeg')

    img_paths = glob.glob(data_dir)
    print(__name__, img_paths)
    recognizer.face_embedding.embed_faces(img_paths, save=2, embedding_path='outputs/finalmodelv1.pickle')
    # recognizer.embedding_path = '../src/outputs/embeddings_duy_new_full_v3.pickle'
    recognizer.update_data()
    recognizer.train('knn')

    # Di chuyển ảnh về folder dataset
    data_dir_new = os.path.join(os.getcwd(), '../dataset/'+str(id))
    for data_dir in glob.glob(os.path.join(os.getcwd(), 'trainFuturetmp/*')):
        # print(data_dir)
        os.system(f'move {data_dir} {data_dir_new}')


if __name__ == '__main__':
    train_classifer('de140217')