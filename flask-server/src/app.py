from ast import Try
from asyncio.windows_events import NULL
import base64
from email.mime import image
import errno
from logging import exception
from pickle import GET
from tkinter import N, X
from tkinter.messagebox import ABORT
from flask import Flask, abort, request , jsonify
from flask_cors import CORS
from flask_jwt_extended import create_access_token,get_jwt,get_jwt_identity, \
                               unset_jwt_cookies, jwt_required, JWTManager
import json
from PIL import Image
import time
import shutil
import os
import io
import cv2
import numpy as np
from datetime import datetime, timedelta, timezone

import detectorImage as faceRecImg
import trainFeature as trn
import postgre as sql


app = Flask(__name__)
CORS(app)

app.config["JWT_SECRET_KEY"] = "facerecognition"
jwt = JWTManager(app)
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=1)

cwd = os.getcwd()

@app.route('/check' , methods = ['POST','GET'])
def check():
    # cap = cv2.VideoCapture(0)
    data = request.get_json()
    admin_id = sql.find_admin_by_id(data['token'])
    if data['devices'] == 'mobile':
        result = data['base64']
        im = Image.open(io.BytesIO(base64.b64decode(result)))
        im.save('datatmp/tmp.jpeg')
        img = cv2.imread("datatmp/tmp.jpeg")
        id , label , mask_percent = faceRecImg.faceRecByImage(img)
        if id == 'unknown':
            name = 'UnKnown'     
        else:
            name = sql.find_user_by_id(id)
        mask_check = (False,True)[label == 'Masked']
        check_status = str(data['check_status'])
        room_id = None if data['room_id'] == "NULL" else  data['room_id']
        sql.insert_check(id,admin_id[0],room_id,check_status,None,mask_check)   
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        return jsonify({
            "user_name": name,
            "mask_status": label,
            "datetime": dt_string,
            "check_status": check_status,
        }) 
    else:
        data = request.get_json()
        result = data['data']
        image_raw=result[result.find('/9'):]
        im = Image.open(io.BytesIO(base64.b64decode(image_raw)))
        im.save('datatmp/tmp.jpeg')
        img = cv2.imread("datatmp/tmp.jpeg")
        admin_id = sql.find_admin_by_id(data['token'])
        
        id , label , mask_percent = faceRecImg.faceRecByImage(img)
        if id == 'unknown':
            name = 'Unknown'     
        else:
            name = sql.find_user_by_id(id)
        mask_check = (False,True)[label == 'Masked']
        check_status = str(data['check_status'])
        room_id = None if data['room_id'] == "NULL" else  data['room_id']
        sql.insert_check(id,admin_id[0],room_id,check_status,None,mask_check)
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        return jsonify({
            "user_name": name,
            "mask_status": label,
            "datetime": dt_string,
            "check_status": check_status,
        }) 
    
@app.route('/addRoom' , methods = ['POST'])
def add_room():

        data = request.get_json()
        des = 'No Description' if data['description'] is None or data['description'] ==''  else data['description']
        insert = sql.insert_room(data['room_name'], data['capacity'], des) 
        print(data['capacity'])  
        if insert > 0:
            return jsonify(sql.get_all_room())
        abort(400)

@app.route('/editRoom' , methods = ['POST','GET'])
def edit_room():
    data = request.get_json()
    print(data)
    edit = sql.update_room(data['room_name'],data['capacity'],data['description'],data['room_id'])
    if edit > 0:
        return jsonify(sql.get_all_room())
    abort(400)

@app.route('/deleteRoom' , methods = ['POST','GET'])
def delete_room():
    data = request.get_json()
    delete = sql.delete_room(data['room_id'])
    if delete > 0:
        return jsonify(sql.get_all_room())
    abort(400)

@app.route('/getRoom' , methods = ['GET'])
def get_all_room():  
    return jsonify(sql.get_all_room())

@app.route('/bookingRoom' , methods = ['POST','GET'])
def booking_room():
    data = request.get_json()
    admin_id = sql.find_admin_by_id(data['admin_id'])
    booking = sql.insert_booking_room(data['room_id'],admin_id[0],data['booking_date'],data['booking_session'])
    if booking > 0:     
        return "room is booked succesfully"
    abort(400)


# @app.route('/checkinmobile' , methods = ['POST','GET'])
# def check_in_mobile():
#     data = request.get_json()
#     result = data['base64']
#     # image_raw = result[result.find('/9'):]
#     im = Image.open(io.BytesIO(base64.b64decode(result)))
#     im.save('datatmp/tmp.jpeg')
#     img = cv2.imread("datatmp/tmp.jpeg")
#     id , label , mask_percent = faceRecImg.faceRecByImage(img)
#     name = sql.find_user_by_id(id)
#     if id != 'unknown':
#         name = sql.find_user_by_id(id)
#         mask_check = (False,True)[label == 'Masked']
#         sql.insert_check(id,1,"in",None,mask_check)   
#         now = datetime.now()
#         dt_string = now.strftime("%d/%m/%Y %H:%M:%S")     
#         return jsonify({
#             "user_name": name,
#             "mask_status": label,
#             "datetime": dt_string,
#             "check_status": "check in"
#         })
#     else:
#         now = datetime.now()
#         dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
#         return jsonify({
#             "user_name": "Unknown",
#             "mask_status": label,
#             "datetime": dt_string,
#             "check_status": "check in",
#             "error" : "error"
#         })


# @app.route('/checkout' , methods = ['POST','GET'])
# def checkout():
#     data = request.get_json()
#     result = data['data']
#     image_raw=result[result.find('/9'):]

#     im = Image.open(io.BytesIO(base64.b64decode(image_raw)))
#     im.save('datatmp/tmp.jpeg')
#     img = cv2.imread("datatmp/tmp.jpeg")
#     id , label , mask_percent = faceRecImg.faceRecByImage(img)
#     admin_id = sql.find_user_by_id_admin(data['token'])

#     if id != 'unknown':
#         name = sql.find_user_by_id(id)
#         mask_check = (False,True)[label == 'Masked']
#         sql.insert_check(id,admin_id[0],"out",None,mask_check)   
#         now = datetime.now()
#         dt_string = now.strftime("%d/%m/%Y %H:%M:%S")     
#         return jsonify({
#             "user_name": name,
#             "mask_status": label,
#             "datetime": dt_string,
#             "check_status": "check out"
#         })
#     else:
#         now = datetime.now()
#         dt_string = now.strftime("%d/%m/%Y %H:%M:%S")     
#         return jsonify({
#             "user_name": "Unknown",
#             "mask_status": label,
#             "datetime": dt_string,
#             "check_status": "check in",
#             "error" : "error"
#         })

# @app.route('/checkoutmobile' , methods = ['POST','GET'])
# def check_out_mobile():
#     data = request.get_json()
#     result = data['base64']
#     # image_raw = result[result.find('/9'):]
#     im = Image.open(io.BytesIO(base64.b64decode(result)))
#     im.save('datatmp/tmp.jpeg')
#     img = cv2.imread("datatmp/tmp.jpeg")
#     id , label , mask_percent = faceRecImg.faceRecByImage(img)
#     name = sql.find_user_by_id(id)
#     if id != 'unknown':
#         name = sql.find_user_by_id(id)
#         mask_check = (False,True)[label == 'Masked']
#         sql.insert_check(id,1,"out",None,mask_check)   
#         now = datetime.now()
#         dt_string = now.strftime("%d/%m/%Y %H:%M:%S")     
#         return jsonify({
#             "user_name": name,
#             "mask_status": label,
#             "datetime": dt_string,
#             "check_status": "check out"
#         })
#     else:
#         now = datetime.now()
#         dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
#         return jsonify({
#             "user_name": "Unknown",
#             "mask_status": label,
#             "datetime": dt_string,
#             "check_status": "check in",
#             "error" : "error"
#         })

@app.route('/trainfeature' , methods = ['POST','GET'])
def trainfeature():
    data = request.get_json()
    
    userid = data['user_id']
    username = data['user_name']
    check_user = sql.find_user_by_id(userid)
    if check_user is not None:
        print(userid)
        abort(400)
            
    else:
        
        i=0
        if data['device'] == 'mobile':
            result = data['base64']
            os.mkdir(os.path.join(cwd,'trainFuturetmp/'+userid))
            for sas in result:
                i = i+1
                im = Image.open(io.BytesIO(base64.b64decode(sas)))
                name = str(i)+'.jpeg'
                im.save('trainFuturetmp/'+userid+'/'+name)
            trn.train_classifer(userid)
            sql.insert_user(userid, username)
            return jsonify({
                "success" : "train ok"
            })
        else:
            result = data['data']    
            os.mkdir(os.path.join(cwd,'trainFuturetmp/'+userid))
            for sas in result:
                i = i+1
                image_raw=sas[sas.find('/9'):]
                im = Image.open(io.BytesIO(base64.b64decode(image_raw)))
                name = str(i)+'.jpeg'
                im.save('trainFuturetmp/'+userid+'/'+name)
            trn.train_classifer(userid)
            sql.insert_user(userid, username)
            return jsonify({
                "success" : "train ok"
            })

            
 

@app.route('/datalog',methods = ['POST','GET'])
def datalog():
    alldata = sql.find_all_log()
    return jsonify(alldata)

@app.route('/getBooking',methods = ['GET'])
def getBooking():
    return jsonify(sql.find_all_booking())

@app.route('/userprofile',methods = ['POST','GET'])
def userprofile():
    data = request.get_json()
    user = sql.find_user_by_id2(data['user_id'].lower())
    print(user)
    return jsonify({
        "user_id":user[0],
         "user_name":user[1]
    })



@app.after_request
def refresh_expiring_jwts(response):
    try:
        exp_timestamp = get_jwt()["exp"]
        now = datetime.now(timezone.utc)
        target_timestamp = datetime.timestamp(now + timedelta(minutes=30))
        if target_timestamp > exp_timestamp:
            access_token = create_access_token(identity=get_jwt_identity())
            data = response.get_json()
            if type(data) is dict:
                data["access_token"] = access_token 
                response.data = json.dumps(data)
        return response
    except (RuntimeError, KeyError):
        # Case where there is not a valid JWT. Just return the original respone
        return response


@app.route('/login', methods=["POST"])
def create_token():
    admin_acc = request.json.get("admin_acc", None)
    password = request.json.get("password", None)
    
    check = sql.find_admin_by_id(admin_acc)

    # print(admin_acc)
    # print(password)
    # print(check) 
    if check is None or (admin_acc != check[1] or password != check[2])  :
        return {"msg": "Wrong email or password"}, 401
    access_token = create_access_token(identity=admin_acc)
    response = {"access_token":access_token}

    return response
    



@app.route("/logout", methods=["POST"])
def logout():
    response = jsonify({"msg": "logout successful"})
    unset_jwt_cookies(response)
    return response



if __name__ == '__main__':
    app.run(debug=False)

